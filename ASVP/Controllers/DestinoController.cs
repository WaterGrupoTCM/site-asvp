﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASVP.Models;

namespace ASVP.Controllers
{
    public class DestinoController : Controller
    {
        private AsiloEntities db = new AsiloEntities();

        //
        // GET: /Destino/

        public ActionResult Index()
        {
            try
            {
                if (VerificaLoginDestino())
                {
                    return View(db.DESTINO.OrderBy(p=>p.descricao).ToList());
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de destinos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }

            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
        }

       
        //
        // GET: /Destino/Details/5

        public ActionResult Details(int id)
        {
            try
            {
                if (VerificaLoginDestino())
                {
                    DESTINO destino = db.DESTINO.Find(id);
                    if (destino == null)
                    {
                        return HttpNotFound();
                    }
                    return View(destino);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de destinos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }

        }

        //
        // GET: /Destino/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Destino/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DESTINO destino)
        {
            try
            {
                if (VerificaLoginDestino())
                {
                    if (ModelState.IsValid)
                    {
                        destino.descricao = destino.descricao.ToUpper();
                        db.DESTINO.Add(destino);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }

                    return View(destino);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de destinos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(destino);

            }
        }

        //
        // GET: /Destino/Edit/5

        public ActionResult Edit(int id)
        {
            try
            {
                if (VerificaLoginDestino())
                {
                    DESTINO destino = db.DESTINO.Find(id);
                    if (destino == null)
                    {
                        return HttpNotFound();
                    }
                    return View(destino);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de destinos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
        }

        //
        // POST: /Destino/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DESTINO destino)
        {
            try
            {
                if (VerificaLoginDestino())
                {
                    if (ModelState.IsValid)
                    {
                        destino.descricao = destino.descricao.ToUpper();
                        db.Entry(destino).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    return View(destino);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de destinos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(destino);

            }
        }

        //
        // GET: /Destino/Delete/5

        public ActionResult Delete(int id)
        {
            try
            {
                if (VerificaLoginDestino())
                {
                    DESTINO destino = db.DESTINO.Find(id);
                    if (destino == null)
                    {
                        return HttpNotFound();
                    }
                    return View(destino);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de destinos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
        }

        //
        // POST: /Destino/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            try
            {
                if (VerificaLoginDestino())
                {
                    DESTINO destino = db.DESTINO.Find(id);
                    db.DESTINO.Remove(destino);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de destinos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public bool VerificaLoginDestino()
        {

            if (Request.Cookies["usuarioAsilo"] != null)
            {
                int id = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString().Replace("usuarioAsilo=", ""));
                if (db.ADMINISTRADOR.Find(id).cadDestino == "S")
                    return true;
                else return false;
            }
            else
            {
                return false;

            }
        }
    }
}