﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASVP.Models;

namespace ASVP.Controllers
{
    public class OperacaoController : Controller
    {
        private AsiloEntities db = new AsiloEntities();

        //
        // GET: /Operacao/

        public ActionResult Index()
        {
            try
            {
                if (VerificaLoginOperacao())
                {
                    ViewBag.idDestino = new SelectList(db.DESTINO, "idDestino", "descricao");
                    return View(db.OPERACAO.OrderBy(p => p.descricao).ToList());
                    
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de operações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }

            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
            
        }

        //
        // GET: /Operacao/Details/5

        public ActionResult Details(int id)
        {
            try
            {
                if (VerificaLoginOperacao())
                {
                    OPERACAO operacao = db.OPERACAO.Find(id);
                    if (operacao == null)
                    {
                        return HttpNotFound();
                    }
                    return View(operacao);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de operações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
            
        }

        //
        // GET: /Operacao/Create

        public ActionResult Create()
        {
            if (VerificaLoginOperacao())
            {
                ViewBag.idDestino = new SelectList(db.DESTINO, "idDestino", "descricao");
                return View();
            }
            else
            {
                TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de operações.";
                return RedirectToAction("Index", "Home", TempData["alertMessage"]);
            }
        }

        //
        // POST: /Operacao/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OPERACAO operacao)
        {
            try
            {
                if (VerificaLoginOperacao())
                {
                    if (ModelState.IsValid)
                    {

                        if (Request.Form["Sim"] != null)
                        {
                            operacao.idoso = "S";
                        }
                        else
                        {
                            if (Request.Form["Nao"] != null)
                            {
                                operacao.idoso = "N";
                            }
                            
                        }
                        
                        operacao.descricao = operacao.descricao.ToUpper();
                        db.OPERACAO.Add(operacao);
                        db.SaveChanges();
                        if (Request.Form["Salvar+"] != null)
                        {

                            Response.Redirect("Create");
                        }
                        else
                        {
                            return RedirectToAction("Index");
                        }
                    }

                    return View(operacao);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de operações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(operacao);

            }
           
        }

        //
        // GET: /Operacao/Edit/5

        public ActionResult Edit(int id)
        {
            try
            {
                if (VerificaLoginOperacao())
                {
                    OPERACAO operacao = db.OPERACAO.Find(id);
                    
                    if (operacao == null)
                    {
                        return HttpNotFound();
                    }
                    return View(operacao);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de operações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
            
        }

        //
        // POST: /Operacao/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OPERACAO operacao)
        {
            try
            {
                if (VerificaLoginOperacao())
                {
                    if (ModelState.IsValid)
                    {

                        if (Request.Form["Sim"] != null)
                        {
                            operacao.idoso = "S";
                        }
                        else
                        {
                            if (Request.Form["Nao"] != null)
                            {
                                operacao.idoso = "N";
                            }

                        }
                        
                        operacao.descricao = operacao.descricao.ToUpper();
                        db.Entry(operacao).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    return View(operacao);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de operações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(operacao);

            }
            
            
        }

        //
        // GET: /Operacao/Delete/5

        public ActionResult Delete(int id)
        {
            try
            {
                if (VerificaLoginOperacao())
                {
                    OPERACAO operacao = db.OPERACAO.Find(id);
                    if (operacao == null)
                    {
                        return HttpNotFound();
                    }
                    return View(operacao);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de operações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
           
        }

        //
        // POST: /Operacao/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                if (VerificaLoginOperacao())
                {
                    OPERACAO operacao = db.OPERACAO.Find(id);
                    db.OPERACAO.Remove(operacao);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de operações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
            
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public bool VerificaLoginOperacao()
        {

            if (Request.Cookies["usuarioAsilo"] != null)
            {
                int id = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString().Replace("usuarioAsilo=", ""));
                if (db.ADMINISTRADOR.Find(id).cadOperacao == "S")
                    return true;
                else return false;
            }
            else
            {
                return false;

            }
        }
    }
}