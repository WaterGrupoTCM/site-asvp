﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASVP.Models;

namespace ASVP.Controllers
{
    public class BancoController : Controller
    {
        private AsiloEntities db = new AsiloEntities();

        //
        // GET: /Banco/

        public ActionResult Index()
        {
            try
            {
                if (VerificaLoginBanco())
                {
                    return View(db.BANCO.OrderBy(p => p.nome).ToList());
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de bancos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {

                ViewBag.Msg = e.Message;
                return View();
            }
            
        }

       

        //
        // GET: /Banco/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                BANCO banco = db.BANCO.Find(id);
                if (banco == null)
                {
                    return HttpNotFound();
                }
                return View(banco);

            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();
            }
           
        }

        //
        // GET: /Banco/Create

        public ActionResult Create()
        {
            try
            {
                if (VerificaLoginBanco())
                {

                    return View();
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de bancos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();
            }
            
        }

        //
        // POST: /Banco/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BANCO banco)
        {
            try
            {
                if (VerificaLoginBanco())
                {
                    if (ModelState.IsValid)
                    {
                        banco.nome = banco.nome.ToUpper();
                        db.BANCO.Add(banco);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }

                    return View(banco);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de bancos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(banco);
            }
           
        }

        //
        // GET: /Banco/Edit/5

        public ActionResult Edit(int id)
        {
            try
            {
                if (VerificaLoginBanco())
                {
                    BANCO banco = db.BANCO.Find(id);
                    if (banco == null)
                    {
                        return HttpNotFound();
                    }
                    return View(banco);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de bancos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();
            }
           
        }

        //
        // POST: /Banco/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BANCO banco)
        {
            try
            {
                if (VerificaLoginBanco())
                {
                    if (ModelState.IsValid)
                    {
                        banco.nome = banco.nome.ToUpper();
                        db.Entry(banco).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    return View(banco);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de bancos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(banco);
            }
            
        }

        //
        // GET: /Banco/Delete/5

        public ActionResult Delete(int id)
        {
            try
            {
                if (VerificaLoginBanco())
                {
                    BANCO banco = db.BANCO.Find(id);
                    if (banco == null)
                    {
                        return HttpNotFound();
                    }
                    return View(banco);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de bancos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();
            }
            
        }

        //
        // POST: /Banco/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                if (VerificaLoginBanco())
                {
                    BANCO banco = db.BANCO.Find(id);
                    db.BANCO.Remove(banco);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de bancos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();
            }
           
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public bool VerificaLoginBanco()
        {

            if (Request.Cookies["usuarioAsilo"] != null)
            {
                int id = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString().Replace("usuarioAsilo=", ""));
                if (db.ADMINISTRADOR.Find(id).cadBanco == "S")
                    return true;
                else return false;
            }
            else
            {
                return false;

            }
        }
    }
}