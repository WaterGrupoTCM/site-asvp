﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASVP.Models;
using PagedList;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.ComponentModel;

namespace ASVP.Controllers
{
    public class IdosoController : Controller
    {
        private AsiloEntities db = new AsiloEntities();

        //
        // GET: /Idoso/

        public ActionResult Index(string idoso, int? pagina, string filtroExcel, string tipoRelatorio)
        {

            try
            {
                if (VerificaLoginIdoso())
                {
                    int tamanhoPagina = 10;
                    int numeroPagina = pagina ?? 1;

                    ViewBag.idoso = idoso;

                    var ListaIdoso = db.IDOSO.Where(p => p.nome == idoso).OrderBy(p => p.nome).ToList();

                    if (tipoRelatorio != null)
                    {
                        if (tipoRelatorio == "Beneficios")
                        {
                            var beneficios = db.BENEFICIO_IDOSO.Include(p => p.IDOSO)
                            .Select(g => new EXPORT_BENEFICIOS
                            {
                                nome = g.IDOSO.nome,
                                cpf = g.IDOSO.cpf,
                                rg = g.IDOSO.rg,
                                pai = g.IDOSO.pai,
                                mae = g.IDOSO.mae,
                                num_beneficio = g.numBeneficio,
                                nNit = g.nNit
                            }).OrderBy(p => p.nome).ToList();

                            ExportBeneficios(beneficios);
                        }
                        else
                        {
                            if (filtroExcel != null)
                                if (filtroExcel != "")
                                    Export(db.IDOSO.OrderBy(p => p.nome).ToList(), filtroExcel);
                        }

                    }

                    if (idoso != null && idoso != "")
                        return View(ListaIdoso.Where(p => p.dataObito == null).ToPagedList(numeroPagina, tamanhoPagina));
                    else
                    {
                        return View(db.IDOSO.Where(p => p.dataObito == null).OrderBy(p => p.nome).ToList().ToPagedList(numeroPagina, tamanhoPagina));
                    }


                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }


        }

        private void Export(List<IDOSO> idosos, string filtroExcel)
        {
            try
            {
                if (VerificaLoginIdoso())
                {
                    foreach (var item in idosos)
                    {

                    }
                    Response.ClearContent();
                    Response.AddHeader("content-disposition",
                        "attachment;filename=Idosos" +
                        DateTime.Now.ToShortDateString().Replace("/", "").Replace("-", "") + ".xls");
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    WriteTsv(idosos, Response.Output, filtroExcel);
                    Response.End();

                    // return RedirectToAction("Index");
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a consulta de movimentos.";
                    // return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                //return View(idosos);

            }


        }

        private void ExportBeneficios(List<EXPORT_BENEFICIOS> beneficios)
        {
            try
            {
                if (VerificaLoginIdoso())
                {
                    foreach (var item in beneficios)
                    {

                    }
                    Response.ClearContent();
                    Response.AddHeader("content-disposition",
                        "attachment;filename=Beneficios" +
                        DateTime.Now.ToShortDateString().Replace("/", "").Replace("-", "") + ".xls");
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    WriteTsvBeneficios(beneficios, Response.Output);
                    Response.End();

                    // return RedirectToAction("Index");
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a consulta de movimentos.";
                    // return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                //return View(idosos);

            }


        }


        //GERA PLANILHA EXCEL
        public void WriteTsv<T>(IEnumerable<T> data, TextWriter output, string filtroExcel)
        {
            try
            {
                PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
                foreach (PropertyDescriptor prop in props)
                {
                    for (int i = 0; i < filtroExcel.Split(',').Length; i++)
                    {
                        if (prop.DisplayName.ToUpper() == filtroExcel.Split(',')[i].Split('-')[0])
                        {
                            output.Write(prop.DisplayName.ToUpper()); // header
                            output.Write("\t");
                        }
                    }

                }
                output.WriteLine();
                foreach (T item in data)
                {
                    foreach (PropertyDescriptor prop in props)
                    {
                        for (int i = 0; i < filtroExcel.Split(',').Length; i++)
                        {
                            if (prop.DisplayName.ToUpper() == filtroExcel.Split(',')[i].Split('-')[0])
                            {
                                output.Write(removeCE(prop.Converter.ConvertToString(prop.GetValue(item))));
                                output.Write("\t");
                            }
                        }

                    }
                    output.WriteLine();
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;


            }

        }

        //GERA PLANILHA EXCEL
        public void WriteTsvBeneficios<T>(IEnumerable<T> data, TextWriter output)
        {
            try
            {
                PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
                foreach (PropertyDescriptor prop in props)
                {
                    output.Write(prop.DisplayName); // header
                    output.Write("\t");
                }
                output.WriteLine();
                foreach (T item in data)
                {
                    foreach (PropertyDescriptor prop in props)
                    {
                        output.Write(removeCE(prop.Converter.ConvertToString(prop.GetValue(item))));
                        output.Write("\t");
                    }
                    output.WriteLine();
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;


            }

        }



        private String removeCE(String str)
        {
            try
            {
                str = str.Replace("Á", "A").Replace("Â", "A").Replace("À", "A")
                        .Replace("Ã", "A").Replace("Ä", "A");
                str = str.Replace("á", "a").Replace("â", "a").Replace("à", "a")
                        .Replace("ã", "a").Replace("ä", "a");
                str = str.Replace("É", "E").Replace("Ê", "E").Replace("È", "E");
                str = str.Replace("é", "e").Replace("ê", "e").Replace("è", "e");
                str = str.Replace("Í", "I").Replace("Î", "I").Replace("Ì", "I")
                        .Replace("Ï", "I");
                str = str.Replace("í", "i").Replace("î", "i").Replace("ì", "i")
                        .Replace("ï", "i");
                str = str.Replace("Ó", "O").Replace("Ô", "O").Replace("Ò", "O")
                        .Replace("Õ", "O");
                str = str.Replace("ó", "o").Replace("ô", "o").Replace("ò", "o")
                        .Replace("õ", "o");
                str = str.Replace("Ú", "U").Replace("Ü", "U");
                str = str.Replace("ú", "u").Replace("ü", "u");
                str = str.Replace("Ç", "C").Replace("ç", "c");

                return str;
            }
            catch (Exception ex)
            {
                return "";
                // MessageBox.Show(")
            }
        }

        public ActionResult TagSearch(string term)
        {
            try
            {
                // Get Tags from database
                var idosos = db.IDOSO.Select(p => p.nome).ToList().Where(t => t.ToUpper().StartsWith(term.ToString().ToUpper()));
                return this.Json(idosos,
                                JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;

                return null;
            }

        }

        //
        // GET: /Idoso/Details/5

        public ActionResult Details(int id = 0)
        {

            try
            {
                if (VerificaLoginIdoso())
                {
                    IDOSO idoso = db.IDOSO.Find(id);
                    if (idoso == null)
                    {
                        return HttpNotFound();
                    }
                    return View(idoso);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }


        }


        //
        // GET: /Idoso/Create

        public ActionResult Create()
        {
            //var beneficios = db.BENEFICIO_IDOSO.Where(p => p.id_idoso == null);

            //if (beneficios != null)
            //{
            //    if (beneficios.Count() > 0)
            //    {
            //        foreach (var item in beneficios)
            //        {
            //            db.BENEFICIO_IDOSO.Remove(item);

            //        }
            //        db.SaveChanges();
            //    }

            //}
            ViewBag.ListaBeneficios = db.BENEFICIO_IDOSO.Where(p => p.id_idoso == null);
            ViewBag.ListaContatos = db.CONTATO_IDOSO.Where(p => p.id_idoso == null);
            return View();
        }

        public PartialViewResult DadosPessoais()
        {
            return PartialView();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBeneficio(BENEFICIO_IDOSO beneficio)
        {

            try
            {
                if (VerificaLoginIdoso())
                {
                    if (ModelState.IsValid)
                    {
                        //    //Profile.Properties = "Teste";

                        //    //db.BENEFICIO_IDOSO.Add(beneficio);
                        //    //db.SaveChanges();
                        //    ViewBag.Beneficio = beneficio;
                        ViewBag.ListaBeneficios = db.BENEFICIO_IDOSO.Where(p => p.id_idoso == null);
                    }

                    return RedirectToAction("Create", "Idoso", ViewBag.Beneficio);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                ViewBag.Beneficio = beneficio;
                return RedirectToAction("Create", "Idoso", new { ViewBag.Beneficio, ViewBag.Msg });

            }


        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddContato(CONTATO_IDOSO contato)
        {

            try
            {
                if (VerificaLoginIdoso())
                {
                    if (ModelState.IsValid)
                    {
                        //Profile.Properties = "Teste";

                        //db.CONTATO_IDOSO.Add(contato);
                        //db.SaveChanges();
                        //ViewBag.Contato = contato;
                        ViewBag.ListaContatos = db.CONTATO_IDOSO.Where(p => p.id_idoso == null);
                    }

                    return RedirectToAction("Create", "Idoso", ViewBag.ListaContatos);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                ViewBag.ListaContatos = contato;
                return RedirectToAction("Create", "Idoso", new { ViewBag.ListaContatos, ViewBag.Msg });

            }


        }

        public ActionResult EditContato(int id)
        {

            try
            {
                if (VerificaLoginIdoso())
                {
                    if (ModelState.IsValid)
                    {
                        CONTATO_IDOSO contato = db.CONTATO_IDOSO.Find(id);
                        ViewBag.Contato = contato;
                    }

                    return RedirectToAction("Create", "Idoso", ViewBag.ListaContatos);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;


                return RedirectToAction("Create", "Idoso", new { ViewBag.ListaContatos, ViewBag.Msg });

            }


        }


        //
        // POST: /Idoso/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IDOSO idoso, CONTATO_IDOSO contato, BENEFICIO_IDOSO beneficio)
        {

            try
            {
                if (VerificaLoginIdoso())
                {
                    //Profile.Properties = "Teste";
                    ViewBag.ListaBeneficios = db.BENEFICIO_IDOSO.Where(p => p.id_idoso == null);
                    ViewBag.ListaContatos = db.CONTATO_IDOSO.Where(p => p.id_idoso == null);


                    if (Request.Form["Salvar"] != null)
                    {
                        if (ModelState.IsValid)
                        {
                            #region tratamento toUpper

                            if (idoso.nome != null)
                                idoso.nome = idoso.nome.ToUpper();

                            if (idoso.naturalidade != null)
                                idoso.naturalidade = idoso.naturalidade.ToUpper();

                            if (idoso.estadoCivil != null)
                                idoso.estadoCivil = idoso.estadoCivil.ToUpper();

                            if (idoso.pai != null)
                                idoso.pai = idoso.pai.ToUpper();

                            if (idoso.mae != null)
                                idoso.mae = idoso.mae.ToUpper();

                            if (idoso.postoSaude != null)
                                idoso.postoSaude = idoso.postoSaude.ToUpper();

                            if (idoso.planosaude != null)
                                idoso.planosaude = idoso.planosaude.ToUpper();

                            if (idoso.leitoPs != null)
                                idoso.leitoPs = idoso.leitoPs.ToUpper();

                            if (idoso.nomeFuneraria != null)
                                idoso.nomeFuneraria = idoso.nomeFuneraria.ToUpper();

                            if (idoso.titularFuneraria != null)
                                idoso.titularFuneraria = idoso.titularFuneraria.ToUpper();

                            if (idoso.obs != null)
                                idoso.obs = idoso.obs.ToUpper();

                            if (idoso.cartorio != null)
                                idoso.cartorio = idoso.cartorio.ToUpper();

                            if (idoso.Procurador != null)
                                idoso.Procurador = idoso.Procurador.ToUpper();

                            if (idoso.curador != null)
                                idoso.curador = idoso.curador.ToUpper();



                            #endregion
                            db.IDOSO.Add(idoso);

                            db.SaveChanges();

                            foreach (var item in db.BENEFICIO_IDOSO.Where(p => p.id_idoso == null))
                            {
                                item.id_idoso = idoso.id_idoso;
                                db.Entry(item).State = EntityState.Modified;

                            }
                            db.SaveChanges();

                            foreach (var item in db.CONTATO_IDOSO.Where(p => p.id_idoso == null))
                            {
                                item.id_idoso = idoso.id_idoso;
                                db.Entry(item).State = EntityState.Modified;

                            }
                            db.SaveChanges();

                            return RedirectToAction("Index");
                        }

                    }
                    else
                    {
                        if (Request.Form["AddContato"] != null)
                        {

                            #region tratamento toUpper

                            if (contato.nomeContato != null)
                                contato.nomeContato = contato.nomeContato.ToUpper();

                            if (contato.grauParentesco != null)
                                contato.grauParentesco = contato.grauParentesco.ToUpper();

                            if (contato.endereco != null)
                                contato.endereco = contato.endereco.ToUpper();

                            if (contato.Bairro != null)
                                contato.Bairro = contato.Bairro.ToUpper();

                            if (contato.cidade != null)
                                contato.cidade = contato.cidade.ToUpper();


                            #endregion

                            db.CONTATO_IDOSO.Add(contato);
                            db.SaveChanges();
                            ViewBag.Contato = new CONTATO_IDOSO();

                            return View(idoso);
                        }
                        else
                        {
                            if (Request.Form["AddBeneficio"] != null)
                            {
                                #region tratamento toUpper

                                if (beneficio.banco != null)
                                    beneficio.banco = beneficio.banco.ToUpper();

                                if (beneficio.agencia != null)
                                    beneficio.agencia = beneficio.agencia.ToUpper();

                                if (beneficio.nConta != null)
                                    beneficio.nConta = beneficio.nConta.ToUpper();

                                #endregion

                                db.BENEFICIO_IDOSO.Add(beneficio);
                                db.SaveChanges();
                                ViewBag.Beneficio = new BENEFICIO_IDOSO();

                                return View(idoso);
                            }
                        }

                    }

                    return RedirectToAction("Create", "Idoso", idoso);




                    return View(idoso);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(idoso);

            }


        }

        //
        // GET: /Idoso/Edit/5

        public ActionResult Edit(int id)
        {

            try
            {
                if (VerificaLoginIdoso())
                {

                    IDOSO idoso = db.IDOSO.Find(id);
                    ViewBag.ListaBeneficios = db.BENEFICIO_IDOSO.Where(p => p.id_idoso == idoso.id_idoso);
                    ViewBag.ListaContatos = db.CONTATO_IDOSO.Where(p => p.id_idoso == idoso.id_idoso);
                    if (idoso == null)
                    {
                        return HttpNotFound();
                    }
                    return View(idoso);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }


        }

        //
        // POST: /Idoso/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(IDOSO idoso, CONTATO_IDOSO contato, BENEFICIO_IDOSO beneficio)
        {

            try
            {
                if (VerificaLoginIdoso())
                {
                    ViewBag.ListaBeneficios = db.BENEFICIO_IDOSO.Where(p => p.id_idoso == idoso.id_idoso);
                    ViewBag.ListaContatos = db.CONTATO_IDOSO.Where(p => p.id_idoso == idoso.id_idoso);

                    if (Request.Form["Salvar"] != null)
                    {

                        if (ModelState.IsValid)
                        {

                            #region tratamento toUpper

                            if (idoso.nome != null)
                                idoso.nome = idoso.nome.ToUpper();

                            if (idoso.naturalidade != null)
                                idoso.naturalidade = idoso.naturalidade.ToUpper();

                            if (idoso.estadoCivil != null)
                                idoso.estadoCivil = idoso.estadoCivil.ToUpper();

                            if (idoso.pai != null)
                                idoso.pai = idoso.pai.ToUpper();

                            if (idoso.mae != null)
                                idoso.mae = idoso.mae.ToUpper();

                            if (idoso.postoSaude != null)
                                idoso.postoSaude = idoso.postoSaude.ToUpper();

                            if (idoso.planosaude != null)
                                idoso.planosaude = idoso.planosaude.ToUpper();

                            if (idoso.leitoPs != null)
                                idoso.leitoPs = idoso.leitoPs.ToUpper();

                            if (idoso.nomeFuneraria != null)
                                idoso.nomeFuneraria = idoso.nomeFuneraria.ToUpper();

                            if (idoso.titularFuneraria != null)
                                idoso.titularFuneraria = idoso.titularFuneraria.ToUpper();

                            if (idoso.obs != null)
                                idoso.obs = idoso.obs.ToUpper();

                            if (idoso.cartorio != null)
                                idoso.cartorio = idoso.cartorio.ToUpper();

                            if (idoso.Procurador != null)
                                idoso.Procurador = idoso.Procurador.ToUpper();

                            if (idoso.curador != null)
                                idoso.curador = idoso.curador.ToUpper();



                            #endregion

                            db.Entry(idoso).State = EntityState.Modified;
                            db.SaveChanges();
                            return RedirectToAction("Index");
                        }

                    }
                    else
                    {
                        if (Request.Form["AddContato"] != null)
                        {
                            if (contato.id_idoso == null)
                                contato.id_idoso = idoso.id_idoso;

                            #region tratamento toUpper

                            if (contato.nomeContato != null)
                                contato.nomeContato = contato.nomeContato.ToUpper();

                            if (contato.grauParentesco != null)
                                contato.grauParentesco = contato.grauParentesco.ToUpper();

                            if (contato.endereco != null)
                                contato.endereco = contato.endereco.ToUpper();

                            if (contato.Bairro != null)
                                contato.Bairro = contato.Bairro.ToUpper();

                            if (contato.cidade != null)
                                contato.cidade = contato.cidade.ToUpper();


                            #endregion

                            db.CONTATO_IDOSO.Add(contato);
                            db.SaveChanges();
                            ViewBag.Contato = new CONTATO_IDOSO();
                            ViewBag.ListaContatos = db.CONTATO_IDOSO.Where(p => p.id_idoso == idoso.id_idoso);
                            return View(idoso);
                        }
                        else
                        {
                            if (Request.Form["AddBeneficio"] != null)
                            {
                                if (beneficio.id_idoso == null)
                                    beneficio.id_idoso = idoso.id_idoso;

                                #region tratamento toUpper

                                if (beneficio.banco != null)
                                    beneficio.banco = beneficio.banco.ToUpper();

                                if (beneficio.agencia != null)
                                    beneficio.agencia = beneficio.agencia.ToUpper();

                                if (beneficio.nConta != null)
                                    beneficio.nConta = beneficio.nConta.ToUpper();

                                #endregion

                                db.BENEFICIO_IDOSO.Add(beneficio);
                                db.SaveChanges();
                                beneficio = null;
                                ViewBag.Beneficio = null;
                                //new BENEFICIO_IDOSO()
                                //{
                                //    numBeneficio = "",
                                //    banco = "",
                                //    agencia = "",
                                //    nConta = "",
                                //    dataCredito = null,
                                //    valorBeneficio = null,
                                //    senha = ""
                                //};
                                ViewBag.ListaBeneficios = db.BENEFICIO_IDOSO.Where(p => p.id_idoso == idoso.id_idoso);
                                return View(idoso);
                            }
                        }

                    }
                    return View(idoso);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(idoso);

            }


        }

        //
        // GET: /Idoso/Delete/5

        public ActionResult Delete(int id)
        {
            try
            {
                if (VerificaLoginIdoso())
                {

                    IDOSO idoso = db.IDOSO.Find(id);
                    if (idoso == null)
                    {
                        return HttpNotFound();
                    }
                    return View(idoso);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }


        }


        public ActionResult DeleteContato(int id, int? id_idoso)
        {
            try
            {
                if (VerificaLoginIdoso())
                {

                    CONTATO_IDOSO contato = db.CONTATO_IDOSO.Find(id);
                    if (contato == null)
                    {
                        return HttpNotFound();
                    }

                    db.CONTATO_IDOSO.Remove(contato);
                    db.SaveChanges();
                    if (id_idoso != null)
                    {
                        ViewBag.ListaContatos = db.CONTATO_IDOSO.Where(p => p.id_idoso == id_idoso);
                        return RedirectToAction("Edit", "Idoso", new { id = id_idoso });
                    }
                    else
                    {
                        ViewBag.ListaContatos = db.CONTATO_IDOSO.Where(p => p.id_idoso == null);
                        return RedirectToAction("Create", "Idoso");
                    }
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }


        }

        public ActionResult DeleteBeneficio(string id, int? id_idoso)
        {
            try
            {
                if (VerificaLoginIdoso())
                {

                    BENEFICIO_IDOSO beneficio = db.BENEFICIO_IDOSO.Find(id);
                    if (beneficio == null)
                    {
                        return HttpNotFound();
                    }

                    db.BENEFICIO_IDOSO.Remove(beneficio);
                    db.SaveChanges();


                    if (id_idoso != null)
                    {

                        ViewBag.ListaBeneficios = db.BENEFICIO_IDOSO.Where(p => p.id_idoso == id_idoso);
                        return RedirectToAction("Edit", "Idoso", new { id = id_idoso });
                    }
                    else
                    {
                        ViewBag.ListaBeneficios = db.BENEFICIO_IDOSO.Where(p => p.id_idoso == null);
                        return RedirectToAction("Create", "Idoso");
                    }
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }


        }
        //
        // POST: /Idoso/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            IDOSO idoso = db.IDOSO.Find(id);
            try
            {
                if (VerificaLoginIdoso())
                {
                    foreach (var item in db.BENEFICIO_IDOSO.Where(p => p.id_idoso == idoso.id_idoso))
                    {
                        db.BENEFICIO_IDOSO.Remove(item);
                    }
                    db.SaveChanges();

                    foreach (var item in db.CONTATO_IDOSO.Where(p => p.id_idoso == idoso.id_idoso))
                    {
                        db.CONTATO_IDOSO.Remove(item);
                    }
                    db.SaveChanges();


                    db.IDOSO.Remove(idoso);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de idosos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {

                ViewBag.Msg = "Existem movimentações vinculadas a " + idoso.nome +
                              ". Para realizar esta tarefa exclua todas as movimentações referentes a este idoso.";
                return View();

            }


        }

        public ActionResult AddPerfil()
        {
            return RedirectToAction("Create", "Idoso");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        public bool VerificaLoginIdoso()
        {

            if (Request.Cookies["usuarioAsilo"] != null)
            {
                int id = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString().Replace("usuarioAsilo=", ""));
                if (db.ADMINISTRADOR.Find(id).cadIdoso == "S")
                    return true;
                else return false;
            }
            else
            {
                return false;

            }
        }

        public class EXPORT_BENEFICIOS
        {
            public string nome { get; set; }
            public string cpf { get; set; }
            public string rg { get; set; }
            public string pai { get; set; }
            public string mae { get; set; }
            public string num_beneficio { get; set; }
            public string nNit { get; set; }
        }



        public FileResult DisplayPDF(int id)
        {

            IDOSO idoso = db.IDOSO.Find(id);

            Document documento = new Document();
            string pCaminhoArquivoPDF = Server.MapPath("~/Relatorio/relatorio.pdf");
            //string pCaminhoArquivoPDF = @"D:\Sistemas\WebSites\ASVP\ASVP\Relatorio\relatorio.pdf";

            System.IO.File.Delete(pCaminhoArquivoPDF);
            PdfWriter writer = PdfWriter.GetInstance(documento, new FileStream(pCaminhoArquivoPDF, FileMode.Append));

            try
            {
                documento.Open();
                // Paragraph p1 = new Paragraph("Nome: Lucas Alves Pereira");
                //documento.Add(p1);
                //Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                //Esqueleto da fatura
                // documento.Add(p);
                PdfContentByte cb = writer.DirectContent;

                cb.MoveTo(documento.PageSize.Width - 30, 30);
                // x = 297.5 - y = 210.5
                cb.LineTo(documento.PageSize.Width - 30, documento.PageSize.Height - 30);
                cb.Stroke();

                cb.MoveTo(documento.PageSize.Width - 30, documento.PageSize.Height - 30);
                // x = 297.5 - y = 210.5
                cb.LineTo(30, documento.PageSize.Height - 30);
                cb.Stroke();


                cb.MoveTo(30, 30); // x = 10 - y = 210.5
                cb.LineTo(documento.PageSize.Width - 30, 30);
                cb.Stroke();

                cb.MoveTo(30, 30); // x = 10 - y = 210.5
                cb.LineTo(30, documento.PageSize.Height - 30);
                cb.Stroke();

                System.Drawing.Image image =
                    System.Drawing.Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "/Images/asilo_logopdf.png");
                iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(image,
                    System.Drawing.Imaging.ImageFormat.Png);
                pdfImage.SetAbsolutePosition(40f, Convert.ToSingle(documento.PageSize.Height - (95)));
                documento.Add(pdfImage);

                ////TÍTULO FICHA CADASTRAL DE IDOSO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 18);
                cb.BeginText();
                cb.SetColorFill(BaseColor.BLACK);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "FICHA CADASTRAL DE IDOSO", 185f, Convert.ToSingle(documento.PageSize.Height - (70)), 0f);
                cb.EndText();
                cb.Stroke();

                ////////////////////////////////////////////////////////////////////////////

                int pos = 60;


                #region PAINEL DADOS PESSOAIS


                cb.SetColorStroke(BaseColor.DARK_GRAY);

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // |
                cb.LineTo(Convert.ToSingle(40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // |
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                cb.Stroke();

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (40 + pos)));
                cb.Stroke();

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (215 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (55 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (55 + pos)));
                cb.Stroke();


                //TITULO DO PAINEL DADOS PESSOAIS
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "DADOS PESSOAIS", 45f, Convert.ToSingle(documento.PageSize.Height - (50 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Nome", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                cb.EndText();
                cb.Stroke();
                if (idoso.nome != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.nome, 48f, Convert.ToSingle(documento.PageSize.Height - (80 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Data Nascimento", 290f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                cb.EndText();
                cb.Stroke();


                if (idoso.dataNasc != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.dataNasc.Value.ToShortDateString(), 290f, Convert.ToSingle(documento.PageSize.Height - (80 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }
                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Idade", 475f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                cb.EndText();
                cb.Stroke();
                int idade = 0;
                if (idoso.dataNasc != null)
                {
                    Nullable<TimeSpan> dias = DateTime.Now - idoso.dataNasc;
                    idade = Convert.ToInt32(dias.Value.Days) / 365;
                }
                cb.BeginText();
                cb.SetColorFill(BaseColor.BLACK);
                cb.ShowTextAligned(Element.ALIGN_LEFT, idade.ToString(), 475f, Convert.ToSingle(documento.PageSize.Height - (80 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Estado Civil", 48f, Convert.ToSingle(documento.PageSize.Height - (95 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.estadoCivil != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.estadoCivil, 48f, Convert.ToSingle(documento.PageSize.Height - (105 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Naturalidade", 120f, Convert.ToSingle(documento.PageSize.Height - (95 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.naturalidade != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.naturalidade, 120f, Convert.ToSingle(documento.PageSize.Height - (105 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }
                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "UF", 290f, Convert.ToSingle(documento.PageSize.Height - (95 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.uf != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.uf, 290f, Convert.ToSingle(documento.PageSize.Height - (105 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }


                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Tp. Sangue", 410f, Convert.ToSingle(documento.PageSize.Height - (95 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.tpSangue != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.tpSangue, 410f, Convert.ToSingle(documento.PageSize.Height - (105 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Grau Dependência", 475f, Convert.ToSingle(documento.PageSize.Height - (95 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.gDependencia != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.gDependencia.ToString(), 475f, Convert.ToSingle(documento.PageSize.Height - (105 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Nome do Pai", 48f, Convert.ToSingle(documento.PageSize.Height - (120 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.pai != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.pai, 48f, Convert.ToSingle(documento.PageSize.Height - (130 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }
                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Nome da Mãe", 290f, Convert.ToSingle(documento.PageSize.Height - (120 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.mae != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.mae, 290f, Convert.ToSingle(documento.PageSize.Height - (130 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }
                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Data Entrada", 48f, Convert.ToSingle(documento.PageSize.Height - (145 + pos)), 0f);
                cb.EndText();
                cb.Stroke();
                if (idoso.dataEntrada != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.dataEntrada.Value.ToShortDateString(), 48f, Convert.ToSingle(documento.PageSize.Height - (155 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Data Óbito", 120f, Convert.ToSingle(documento.PageSize.Height - (145 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.dataObito != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.dataObito.Value.ToShortDateString(), 120f, Convert.ToSingle(documento.PageSize.Height - (155 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                pos += 120;

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // |
                cb.LineTo(Convert.ToSingle(45), Convert.ToSingle(documento.PageSize.Height - (90 + pos)));
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // |
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (90 + pos)));
                cb.Stroke();

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (40 + pos)));
                cb.Stroke();

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (90 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (90 + pos)));

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (53 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (53 + pos)));
                cb.Stroke();


                //TITULO DO PAINEL VESTIMENTA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Vestimenta", 48f, Convert.ToSingle(documento.PageSize.Height - (50 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                pos -= 80;

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Tam. Camisa", 48f, Convert.ToSingle(documento.PageSize.Height - (145 + pos)), 0f);
                cb.EndText();
                cb.Stroke();
                if (idoso.tamanhoCamisa != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.tamanhoCamisa, 48f, Convert.ToSingle(documento.PageSize.Height - (155 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Tam. Calça", 120f, Convert.ToSingle(documento.PageSize.Height - (145 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.tamanhoCalca != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.tamanhoCalca, 120f, Convert.ToSingle(documento.PageSize.Height - (155 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Tam. Calçado", 190f, Convert.ToSingle(documento.PageSize.Height - (145 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.tamanhoCalcado != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.tamanhoCalcado, 190f, Convert.ToSingle(documento.PageSize.Height - (155 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }






                #endregion


                pos += 140;
                #region PAINEL PERFIL


                cb.SetColorStroke(BaseColor.DARK_GRAY);

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // |
                cb.LineTo(Convert.ToSingle(40), Convert.ToSingle(documento.PageSize.Height - (100 + pos)));
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // |
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (100 + pos)));
                cb.Stroke();

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (40 + pos)));
                cb.Stroke();

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (100 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (100 + pos)));

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (55 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (55 + pos)));
                cb.Stroke();


                //TITULO DO PAINEL PERFIL
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "PERFIL", 45f, Convert.ToSingle(documento.PageSize.Height - (50 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Condições de Saúde", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.perfil != null)
                {
                    if (idoso.perfil.Length > 78)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.perfil.Replace("%", ", ").Substring(0, 83).Trim(), 48f, Convert.ToSingle(documento.PageSize.Height - (80 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();

                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.perfil.Replace("%", ", ").Substring(83, idoso.perfil.Replace("%", ", ").Length - 85).Trim(), 48f, Convert.ToSingle(documento.PageSize.Height - (90 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }
                    else
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.perfil.Replace("%", ", ").Trim(), 48f, Convert.ToSingle(documento.PageSize.Height - (80 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }
                }



                #endregion

                pos -= 60;

                #region DOCUMENTOS

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // |
                cb.LineTo(Convert.ToSingle(40), Convert.ToSingle(documento.PageSize.Height - (330 + pos)));
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // |
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (330 + pos)));
                cb.Stroke();

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (170 + pos)));
                cb.Stroke();

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (330 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (330 + pos)));

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (185 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (185 + pos)));
                cb.Stroke();


                //TITULO DO PAINEL DOCUMENTOS
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "DOCUMENTOS", 45f, Convert.ToSingle(documento.PageSize.Height - (180 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "CPF", 48f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                cb.EndText();
                cb.Stroke();
                if (idoso.cpf != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.cpf, 48f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "RG", 190f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.rg != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.rg, 190f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Data Emissão RG", 290f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.dtEmissaoRG != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.dtEmissaoRG.Value.ToShortDateString(), 290f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Título de Eleitor", 390f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.tituloEleitor != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.tituloEleitor, 390f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }
                #region Caixa Certidão de Nascimento
                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (215 + pos))); // |
                cb.LineTo(Convert.ToSingle(45), Convert.ToSingle(documento.PageSize.Height - (255 + pos)));
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (215 + pos))); // |
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (255 + pos)));
                cb.Stroke();

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (215 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                cb.Stroke();

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (255 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (255 + pos)));

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (228 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (228 + pos)));
                cb.Stroke();


                //TITULO DA CAIXA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.BLACK);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Certidão de Nascimento", 48f, Convert.ToSingle(documento.PageSize.Height - (224 + pos)), 0f);
                cb.EndText();
                cb.Stroke();


                pos -= 1;
                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Nº", 48f, Convert.ToSingle(documento.PageSize.Height - (240 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.certNascN != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.certNascN, 48f, Convert.ToSingle(documento.PageSize.Height - (250 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Livro", 120f, Convert.ToSingle(documento.PageSize.Height - (240 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.certNascL != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.certNascL, 120f, Convert.ToSingle(documento.PageSize.Height - (250 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Folha", 190f, Convert.ToSingle(documento.PageSize.Height - (240 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.certNascF != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.certNascF, 190f, Convert.ToSingle(documento.PageSize.Height - (250 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Cartório", 260f, Convert.ToSingle(documento.PageSize.Height - (240 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.cartorio != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.cartorio, 260f, Convert.ToSingle(documento.PageSize.Height - (250 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }
                #endregion

                #region Caixa Procuração
                pos += 50;

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (215 + pos))); // |
                cb.LineTo(Convert.ToSingle(45), Convert.ToSingle(documento.PageSize.Height - (278 + pos)));
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (215 + pos))); // |
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (278 + pos)));
                cb.Stroke();

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (215 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                cb.Stroke();

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (278 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (278 + pos)));

                cb.MoveTo(45, Convert.ToSingle(documento.PageSize.Height - (228 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 45), Convert.ToSingle(documento.PageSize.Height - (228 + pos)));
                cb.Stroke();


                //TITULO DA CAIXA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.BLACK);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Procuração", 48f, Convert.ToSingle(documento.PageSize.Height - (224 + pos)), 0f);
                cb.EndText();
                cb.Stroke();


                pos -= 1;
                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Nº", 48f, Convert.ToSingle(documento.PageSize.Height - (240 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.ProcuracaoN != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.ProcuracaoN, 48f, Convert.ToSingle(documento.PageSize.Height - (250 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }
                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Livro", 120f, Convert.ToSingle(documento.PageSize.Height - (240 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.ProcuracaoL != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.ProcuracaoL, 120f, Convert.ToSingle(documento.PageSize.Height - (250 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Folha", 190f, Convert.ToSingle(documento.PageSize.Height - (240 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.ProcuracaoF != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.ProcuracaoF, 190f, Convert.ToSingle(documento.PageSize.Height - (250 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Data", 290f, Convert.ToSingle(documento.PageSize.Height - (240 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.dataProcuracao != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.dataProcuracao.Value.ToShortDateString(), 290f, Convert.ToSingle(documento.PageSize.Height - (250 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Procurador", 48f, Convert.ToSingle(documento.PageSize.Height - (265 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.Procurador != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.Procurador, 48f, Convert.ToSingle(documento.PageSize.Height - (275 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Curador", 290f, Convert.ToSingle(documento.PageSize.Height - (265 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.curador != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.curador, 290f, Convert.ToSingle(documento.PageSize.Height - (275 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }
                #endregion

                #endregion

                pos += 120;

                #region SAÚDE

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // |
                cb.LineTo(Convert.ToSingle(40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // |
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                cb.Stroke();

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (170 + pos)));
                cb.Stroke();

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (215 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (185 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (185 + pos)));
                cb.Stroke();


                //TITULO DO PAINEL SAÚDE
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "SAÚDE", 45f, Convert.ToSingle(documento.PageSize.Height - (180 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Posto de Saúde", 48f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                cb.EndText();
                cb.Stroke();
                if (idoso.postoSaude != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.postoSaude, 48f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Cartão SUS", 190f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.cartaoSus != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.cartaoSus, 190f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Plano de Saúde", 290f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.planosaude != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.planosaude, 290f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Leito", 390f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.leitoPs != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.leitoPs, 390f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }



                #endregion


                pos += 50;
                #region FUNERÁRIA

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // |
                cb.LineTo(Convert.ToSingle(40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // |
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                cb.Stroke();

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (170 + pos)));
                cb.Stroke();

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (215 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));

                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (185 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (185 + pos)));
                cb.Stroke();


                //TITULO DO PAINEL FUNERÁRIA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "FUNERÁRIA", 45f, Convert.ToSingle(documento.PageSize.Height - (180 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Nome", 48f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                cb.EndText();
                cb.Stroke();
                if (idoso.nomeFuneraria != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.nomeFuneraria, 48f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }

                cb.BeginText();
                cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Titular", 290f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                cb.EndText();
                cb.Stroke();

                if (idoso.titularFuneraria != null)
                {
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.titularFuneraria, 290f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                }



                #endregion


                List<BENEFICIO_IDOSO> beneficios = db.BENEFICIO_IDOSO.Where(p => p.id_idoso == id).OrderBy(p => p.numBeneficio).ToList();
                if (beneficios.Count > 0)
                {
                    pos += 50;

                    #region BENEFÍCIOS


                    cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // _
                    cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (170 + pos)));
                    cb.Stroke();


                    cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (185 + pos))); // _
                    cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (185 + pos)));
                    cb.Stroke();


                    //TITULO DO PAINEL BENEFÍCIOS
                    cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.DARK_GRAY);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "BENEFÍCIOS", 45f, Convert.ToSingle(documento.PageSize.Height - (180 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Nº", 48f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Banco", 148f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Agência", 250f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Conta", 300f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Nº NIT", 400f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();



                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Data Crédito", 480f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    int i = 0;
                    foreach (var beneficio in beneficios)
                    {
                        if (i < 4)
                        {
                            #region BENEFÍCIOS
                            if (beneficio.numBeneficio != null)
                            {
                                cb.BeginText();
                                cb.SetColorFill(BaseColor.BLACK);
                                cb.ShowTextAligned(Element.ALIGN_LEFT, beneficio.numBeneficio, 48f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                                cb.EndText();
                                cb.Stroke();
                            }


                            if (beneficio.banco != null)
                            {
                                cb.BeginText();
                                cb.SetColorFill(BaseColor.BLACK);
                                cb.ShowTextAligned(Element.ALIGN_LEFT, beneficio.banco, 148f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                                cb.EndText();
                                cb.Stroke();
                            }



                            if (beneficio.agencia != null)
                            {
                                cb.BeginText();
                                cb.SetColorFill(BaseColor.BLACK);
                                cb.ShowTextAligned(Element.ALIGN_LEFT, beneficio.agencia, 250f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                                cb.EndText();
                                cb.Stroke();
                            }



                            if (beneficio.nConta != null)
                            {
                                cb.BeginText();
                                cb.SetColorFill(BaseColor.BLACK);
                                cb.ShowTextAligned(Element.ALIGN_LEFT, beneficio.nConta, 300f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                                cb.EndText();
                                cb.Stroke();
                            }

                            if (beneficio.nNit != null)
                            {
                                cb.BeginText();
                                cb.SetColorFill(BaseColor.BLACK);
                                cb.ShowTextAligned(Element.ALIGN_LEFT, beneficio.nNit, 400f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                                cb.EndText();
                                cb.Stroke();
                            }


                            if (beneficio.dataCredito != null)
                            {
                                cb.BeginText();
                                cb.SetColorFill(BaseColor.BLACK);
                                cb.ShowTextAligned(Element.ALIGN_LEFT, beneficio.dataCredito.ToString(), 480f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                                cb.EndText();
                                cb.Stroke();
                            }

                            #endregion
                            pos += 13;
                        }
                        i++;

                    }

                    int tamanho = beneficios.Count;
                    if (tamanho > 4)
                        tamanho = 4;

                    cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (170 - ((tamanho * 13) - pos)))); // |
                    cb.LineTo(Convert.ToSingle(40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                    cb.Stroke();

                    cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (170 - ((tamanho * 13) - pos)))); // |
                    cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                    cb.Stroke();
                    cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (215 + pos))); // _
                    cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (215 + pos)));
                    cb.Stroke();
                    #endregion

                }


                List<CONTATO_IDOSO> contatos = db.CONTATO_IDOSO.Where(p => p.id_idoso == id).OrderBy(p => p.nomeContato).ToList();
                if (contatos.Count > 0)
                {
                    pos += 50;

                    #region CONTATOS
                    cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (170 + pos))); // _
                    cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (170 + pos)));
                    cb.Stroke();

                    cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (185 + pos))); // _
                    cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (185 + pos)));
                    cb.Stroke();


                    //TITULO DO PAINEL CONTATOS
                    cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.DARK_GRAY);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "CONTATOS", 45f, Convert.ToSingle(documento.PageSize.Height - (180 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();


                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Nome", 48f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Parentesco", 250f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Telefone", 340f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();


                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Celular", 450f, Convert.ToSingle(documento.PageSize.Height - (200 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    int i = 0;
                    foreach (var contato in contatos)
                    {
                        if (i < 4)
                        {
                            #region CONTATOS

                            if (contato.nomeContato != null)
                            {
                                cb.BeginText();
                                cb.SetColorFill(BaseColor.BLACK);
                                cb.ShowTextAligned(Element.ALIGN_LEFT, contato.nomeContato, 48f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                                cb.EndText();
                                cb.Stroke();
                            }



                            if (contato.grauParentesco != null)
                            {
                                cb.BeginText();
                                cb.SetColorFill(BaseColor.BLACK);
                                cb.ShowTextAligned(Element.ALIGN_LEFT, contato.grauParentesco, 250f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                                cb.EndText();
                                cb.Stroke();
                            }



                            if (contato.telefone1 != null)
                            {
                                cb.BeginText();
                                cb.SetColorFill(BaseColor.BLACK);
                                cb.ShowTextAligned(Element.ALIGN_LEFT, contato.telefone1, 340f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                                cb.EndText();
                                cb.Stroke();
                            }


                            if (contato.celular != null)
                            {
                                cb.BeginText();
                                cb.SetColorFill(BaseColor.BLACK);
                                cb.ShowTextAligned(Element.ALIGN_LEFT, contato.celular, 450f, Convert.ToSingle(documento.PageSize.Height - (210 + pos)), 0f);
                                cb.EndText();
                                cb.Stroke();
                            }



                            #endregion

                            pos += 13;
                        }
                        i++;
                    }

                    int tamanho = contatos.Count;
                    if (tamanho > 4)
                        tamanho = 4;

                    cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (170 - ((tamanho * 13) - pos)))); // |
                    cb.LineTo(Convert.ToSingle(40), Convert.ToSingle(documento.PageSize.Height - (205 + pos)));
                    cb.Stroke();

                    cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (170 - ((tamanho * 13) - pos)))); // |
                    cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (205 + pos)));
                    cb.Stroke();

                    cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (205 + pos))); // _
                    cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (205 + pos)));
                    cb.Stroke();
                    #endregion
                }


                documento.Close();

                writer.Close();






                return File(pCaminhoArquivoPDF, "application/pdf");

            }
            catch (Exception e)
            {
                //MessageBox.Show(e.StackTrace);
                if (documento.IsOpen())
                    documento.Close();
                return null;
            }

        }

        public FileResult Print()
        {



            Document documento = new Document();
            string pCaminhoArquivoPDF = Server.MapPath("~/Relatorio/print.pdf");
            
            

            System.IO.File.Delete(pCaminhoArquivoPDF);
            PdfWriter writer = PdfWriter.GetInstance(documento, new FileStream(pCaminhoArquivoPDF, FileMode.Append));

            try
            {
                documento.Open();
                // Paragraph p1 = new Paragraph("Nome: Lucas Alves Pereira");
                //documento.Add(p1);
                //Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                //Esqueleto da fatura
                // documento.Add(p);
                PdfContentByte cb = writer.DirectContent;

                cb.MoveTo(documento.PageSize.Width - 30, 30);
                // x = 297.5 - y = 210.5
                cb.LineTo(documento.PageSize.Width - 30, documento.PageSize.Height - 30);
                cb.Stroke();

                cb.MoveTo(documento.PageSize.Width - 30, documento.PageSize.Height - 30);
                // x = 297.5 - y = 210.5
                cb.LineTo(30, documento.PageSize.Height - 30);
                cb.Stroke();


                cb.MoveTo(30, 30); // x = 10 - y = 210.5
                cb.LineTo(documento.PageSize.Width - 30, 30);
                cb.Stroke();

                cb.MoveTo(30, 30); // x = 10 - y = 210.5
                cb.LineTo(30, documento.PageSize.Height - 30);
                cb.Stroke();

                System.Drawing.Image image =
                    System.Drawing.Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "/Images/asilo_logopdf.png");
                iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(image,
                    System.Drawing.Imaging.ImageFormat.Png);
                pdfImage.SetAbsolutePosition(40f, Convert.ToSingle(documento.PageSize.Height - (95)));
                documento.Add(pdfImage);

                ////TÍTULO RELATORIO DE IDOSO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 18);
                cb.BeginText();
                cb.SetColorFill(BaseColor.BLACK);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "RELATÓRIO DE IDOSOS", 215f, Convert.ToSingle(documento.PageSize.Height - (70)), 0f);
                cb.EndText();
                cb.Stroke();


                ////SUBTÍTULO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.BLACK);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Asilo São Vicente de Paulo", 48f, Convert.ToSingle(documento.PageSize.Height - (105)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.BLACK);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Documentação de Idosos - Emitido em "+DateTime.Now.ToShortDateString(), 48f, Convert.ToSingle(documento.PageSize.Height - (115)), 0f);
                cb.EndText();
                cb.Stroke();

                ////////////////////////////////////////////////////////////////////////////

                int pos = 60;
                int contadorIdoso = 0;
                foreach (var idoso in db.IDOSO.OrderBy(p => p.nome))
                {
                    contadorIdoso++;
                    cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Codigo/Nome".PadRight(25, '.') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.nome != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.id_idoso.ToString().PadLeft(5, '0') + " - " + idoso.nome, 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    pos += 10;

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Data Nascimento".PadRight(25, '.') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.dataNasc != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.dataNasc.Value.ToShortDateString(), 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }
                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Tipo benefício".PadRight(25, '.') + ":", 360f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.planosaude != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.planosaude, 460f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    pos += 10;

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Pai".PadRight(25, '.') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.pai != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.pai, 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    pos += 10;

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Mãe".PadRight(25, '.') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.mae != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.mae, 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Leito.:".PadRight(25, '.') + ":", 360f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.leitoPs != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.leitoPs, 460f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    pos += 10;

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Aposentado".PadRight(25, '.') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.aposentado != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.aposentado, 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Data de admissão".PadRight(25, '.') + ":", 360f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.dataEntrada != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.dataEntrada.Value.ToShortDateString(), 460f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    pos += 10;

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Vaga SUAS".PadRight(25, '.') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.vagasuas != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.vagasuas, 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Sexo".PadRight(25, '.') + ":", 360f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.sexo != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.sexo, 460f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }
                    int i = 0;
                    string banco = "";
                    string cartao1 = "";
                    string cartao2 = "";
                    
                    foreach (var beneficio in db.BENEFICIO_IDOSO.Where(p => p.id_idoso == idoso.id_idoso))
                    {
                        pos += 10;
                        i++;
                        cb.BeginText();
                        cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                        cb.ShowTextAligned(Element.ALIGN_LEFT, ("Beneficio " + i).ToString().PadRight(25, ' ') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                        if (beneficio.numBeneficio != null)
                        {
                            cb.BeginText();
                            cb.SetColorFill(BaseColor.BLACK);
                            cb.ShowTextAligned(Element.ALIGN_LEFT, beneficio.numBeneficio, 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                            cb.EndText();
                            cb.Stroke();
                        }

                        cb.BeginText();
                        cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                        cb.ShowTextAligned(Element.ALIGN_LEFT, "NIT".PadRight(25, '.') + ":", 360f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                        if (beneficio.nNit != null)
                        {
                            cb.BeginText();
                            cb.SetColorFill(BaseColor.BLACK);
                            cb.ShowTextAligned(Element.ALIGN_LEFT, beneficio.nNit, 460f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                            cb.EndText();
                            cb.Stroke();
                        }
                        if (i == 1)
                        {
                            if (beneficio.cartao != null)
                                cartao1 = beneficio.cartao;
                        }
                        else
                            if (beneficio.cartao != null)
                                cartao2 = beneficio.cartao;

                        if (beneficio.banco != null)
                            banco = beneficio.banco;
                    }


                    pos += 10;
                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, ("Banco pagador").ToString().PadRight(25, ' ') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (banco != "")
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, banco, 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Cartão 1".PadRight(25, '.') + ":", 360f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (cartao1 != "")
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, cartao1, 460f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }


                    pos += 10;
                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, ("CPF").ToString().PadRight(25, ' ') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.cpf != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.cpf, 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Cartão 2".PadRight(25, '.') + ":", 360f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (cartao2 != "")
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, cartao2, 460f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    pos += 10;
                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, ("RG").ToString().PadRight(25, ' ') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.rg != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.rg, 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Emissão".PadRight(25, '.') + ":", 360f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.dtEmissaoRG != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.dtEmissaoRG.Value.ToShortDateString(), 460f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    pos += 10;
                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, ("Procuração do INSS").ToString().PadRight(25, ' ') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.Procurador != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, "S", 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Revalidação INSS".PadRight(25, '.') + ":", 360f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    /*if (idoso.dtEmissaoRG != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.dtEmissaoRG.Value.ToShortDateString(), 460f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }*/

                    pos += 10;
                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, ("Procurador").ToString().PadRight(25, ' ') + ":", 48f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.Procurador != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.Procurador, 148f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    cb.BeginText();
                    cb.SetColorFill(new CMYKColor(1f, 88f, 0f, 0f));
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Data".PadRight(25, ' ') + ":", 360f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                    cb.EndText();
                    cb.Stroke();
                    if (idoso.dataProcuracao != null)
                    {
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.BLACK);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, idoso.dataProcuracao.Value.ToShortDateString(), 460f, Convert.ToSingle(documento.PageSize.Height - (70 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    


                    pos += 30;


                    if (pos > 630 && contadorIdoso < db.IDOSO.Count())
                    {
                        documento.NewPage();

                        cb.MoveTo(documento.PageSize.Width - 30, 30);
                        // x = 297.5 - y = 210.5
                        cb.LineTo(documento.PageSize.Width - 30, documento.PageSize.Height - 30);
                        cb.Stroke();

                        cb.MoveTo(documento.PageSize.Width - 30, documento.PageSize.Height - 30);
                        // x = 297.5 - y = 210.5
                        cb.LineTo(30, documento.PageSize.Height - 30);
                        cb.Stroke();


                        cb.MoveTo(30, 30); // x = 10 - y = 210.5
                        cb.LineTo(documento.PageSize.Width - 30, 30);
                        cb.Stroke();

                        cb.MoveTo(30, 30); // x = 10 - y = 210.5
                        cb.LineTo(30, documento.PageSize.Height - 30);
                        cb.Stroke();
                        pos = 0;


                    }
                    else
                    {
                       
                        cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (70 + pos-20))); // _
                        cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40), Convert.ToSingle(documento.PageSize.Height - (70 + pos-20)));
                        cb.Stroke();
                    }


                    
                }
                documento.Close();

                writer.Close();
                
                return File(pCaminhoArquivoPDF, "application/pdf");

            }
            catch (Exception e)
            {
                //MessageBox.Show(e.StackTrace);
                if (documento.IsOpen())
                    documento.Close();
                return null;
            }

        }
    }

}