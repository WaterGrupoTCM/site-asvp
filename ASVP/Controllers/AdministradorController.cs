﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASVP.Models;

namespace ASVP.Controllers
{
    public class AdministradorController : Controller
    {
        private AsiloEntities db = new AsiloEntities();

        //
        // GET: /Administrador/

        public ActionResult Index()
        {

            try
            {
                if (VerificaLoginADM())
                {
                    return View(db.ADMINISTRADOR.OrderBy(p=>p.nome).ToList());
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de configurações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
        }

       

        //
        // GET: /Administrador/Details/5

        public ActionResult Details(int id)
        {
            try
            {
                if (VerificaLoginADM())
                {
                    ADMINISTRADOR administrador = db.ADMINISTRADOR.Find(id);
                    if (administrador == null)
                    {
                        return HttpNotFound();
                    }
                    return View(administrador);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de configurações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
            
        }

        //
        // GET: /Administrador/Create

        public ActionResult Create()
        {
            try
            {
                if (VerificaLoginADM())
                {
                    return View();
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de configurações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
            
        }

        //
        // POST: /Administrador/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ADMINISTRADOR administrador)
        {
            try
            {
                if (VerificaLoginADM())
                {
                    if (ModelState.IsValid)
                    {
                        db.ADMINISTRADOR.Add(administrador);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }

                    return View(administrador);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de configurações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
            
        }

        //
        // GET: /Administrador/Edit/5

        public ActionResult Edit(int id)
        {
            try
            {
                if (VerificaLoginADM())
                {
                    ADMINISTRADOR administrador = db.ADMINISTRADOR.Find(id);
                    if (administrador == null)
                    {
                        return HttpNotFound();
                    }
                    return View(administrador);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de configurações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
           
        }

        //
        // POST: /Administrador/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ADMINISTRADOR administrador)
        {
            try
            {
                if (VerificaLoginADM())
                {
                    if (ModelState.IsValid)
                    {
                        db.Entry(administrador).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    return View(administrador);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de configurações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
           
        }

        //
        // GET: /Administrador/Delete/5

        public ActionResult Delete(int id)
        {
            try
            {
                if (VerificaLoginADM())
                {
                    ADMINISTRADOR administrador = db.ADMINISTRADOR.Find(id);
                    if (administrador == null)
                    {
                        return HttpNotFound();
                    }
                    return View(administrador);
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de configurações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
            
        }

        //
        // POST: /Administrador/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                if (VerificaLoginADM())
                {
                    ADMINISTRADOR administrador = db.ADMINISTRADOR.Find(id);
                    db.ADMINISTRADOR.Remove(administrador);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de configurações.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }
            
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public bool VerificaLoginADM()
        {

            if (Request.Cookies["usuarioAsilo"] != null)
            {
                int id = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString().Replace("usuarioAsilo=", ""));
                if (db.ADMINISTRADOR.Find(id).administrador1 == "S")
                    return true;
                else return false;
            }
            else
            {
                return false;

            }
        }
    }
}