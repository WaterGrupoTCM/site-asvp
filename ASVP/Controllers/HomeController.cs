﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using ASVP.Models;
using PagedList;
using System.Data.Objects;

namespace ASVP.Controllers
{
    public class HomeController : Controller
    {
        AsiloEntities db = new AsiloEntities();
        public ActionResult Index(int? pagina)
        {
            
            if (VerificaLogin())
            {
               
                try
                {
                    int tamanhoPagina = 40;
                    int numeroPagina = pagina ?? 1;
                    var idoso = db.IDOSO.ToList().Where(p => p.dataNasc != null);
                    idoso = idoso.Where(p => p.dataNasc.Value.Month == DateTime.Now.Month);
                    var saldoAtual = db.MOVIMENTO.Include(m => m.IDOSO).Include(m => m.OPERACAO);
                    int usuario = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString().Replace("usuarioAsilo=", ""));
                    ADMINISTRADOR adm =
                        db.ADMINISTRADOR.First(p => p.id_usuario == usuario);
                    ViewBag.Message = " " + adm.nome;
                    ViewBag.SaldoAtual = saldoAtual;
                    ViewBag.ConsultaMovimento = VerificaLoginConsMovimento();
                    ViewData["Movimentos"] = BuscaMovimentosDia().ToPagedList(numeroPagina, tamanhoPagina);


                    return View(idoso.OrderBy(p=>p.dataNasc));
                }
                catch (Exception e)
                {
                    ViewBag.Msg = e.Message;
                    return View();

                }
                
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

        }

        public bool VerificaLoginConsMovimento()
        {

            if (Request.Cookies["usuarioAsilo"] != null)
            {
                int id = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString().Replace("usuarioAsilo=", ""));
                if (db.ADMINISTRADOR.Find(id).consMovVencidos == "S")
                    return true;
                else return false;
            }
            else
            {
                return false;

            }
        }

        public PartialViewResult ListarMovimentos()
        {
            return PartialView(BuscaMovimentosDia());
        }

        public List<MOVIMENTO> BuscaMovimentosDia()
        {
            try
            {
                DateTime dataatual = DateTime.Now.Date;
                var movimentos = db.MOVIMENTO.Where(p => EntityFunctions.TruncateTime(p.dataVenc) == dataatual.Date).ToList();
                //var movimentos = db.MOVIMENTO.ToList();
                return movimentos;
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return null;
            }



        }

        public bool VerificaLogin()
        {

            if (Request.Cookies["usuarioAsilo"] != null)
            {
                return true;
            }
            else
            {
            return false;
                
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
