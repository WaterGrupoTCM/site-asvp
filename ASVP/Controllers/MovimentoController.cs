﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASVP.Models;
using System.Data.Objects.SqlClient;
using PagedList;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using System.Drawing;
using System.Globalization;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace ASVP.Controllers
{
    public class MovimentoController : Controller
    {
        private AsiloEntities db = new AsiloEntities();

        //
        // GET: /Movimento/


        public ActionResult Index(string dataInicio, string dataFim, string TipoOperacao, string id_operacao, string idoso, string order, string pesquisaData,
 string id_banco, int? pagina, string pgtoEfetuado, string pgtoAberto, string idDestino, string grafico, string legenda)
        {
            try
            {
                if (VerificaLoginConsMovimento())
                {

                    int tamanhoPagina = 40;
                    int numeroPagina = pagina ?? 1;


                    int dias = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
                    if (dataInicio == null && dataFim == null)
                    {
                        dataInicio = "01" + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" +
                                     DateTime.Now.Year.ToString();
                        dataFim = dias.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') +
                                  "/" + DateTime.Now.Year.ToString();
                    }


                    var movimentacoes = db.MOVIMENTO.Include(m => m.IDOSO).Include(m => m.OPERACAO).Include(m => m.BANCO).Include(m => m.DESTINO);
                    var saldoAtual = db.MOVIMENTO.Include(m => m.IDOSO).Include(m => m.OPERACAO).Include(m => m.BANCO);

                    #region Filtros


                    
                    

                    if (id_banco != null)
                    {

                        if (id_banco != "")
                        {
                            int banco = Convert.ToInt32(id_banco);
                            movimentacoes = movimentacoes.Where(p => p.id_banco == banco);
                        }

                    }

                    if (TipoOperacao != null)
                    {
                        if (TipoOperacao.Trim() != "tipo")
                        {

                            movimentacoes = movimentacoes.Where(p => p.OPERACAO.tipo == TipoOperacao);
                        }
                    }
                    if (id_operacao != null)
                    {
                        if (id_operacao != "")
                        {
                            int idOperacao = Convert.ToInt32(id_operacao);
                            movimentacoes = movimentacoes.Where(p => p.id_operacao == idOperacao);

                        }

                    }
                    if (idDestino != null)
                    {
                        if (idDestino != "")
                        {
                            int idDest = Convert.ToInt32(idDestino);
                            movimentacoes = movimentacoes.Where(p => p.idDestino == idDest);

                        }

                    }
                    if (idoso != null)
                    {
                        if (idoso != "")
                        {
                            int codigoidoso = Convert.ToInt32(idoso.Split('-')[0]);

                            IDOSO ObjIdoso = db.IDOSO.First(p => p.id_idoso == codigoidoso);
                            movimentacoes = movimentacoes.Where(p => p.id_idoso == ObjIdoso.id_idoso);

                        }

                    }
                    if (TipoOperacao != null)
                    {
                        if (TipoOperacao.Trim() != "tipo")
                            ViewBag.id_operacao = new SelectList(db.OPERACAO.Where(p => p.tipo == TipoOperacao).OrderBy(p => p.descricao),
                                "id_operacao", "descricao");
                        else
                        {
                            ViewBag.id_operacao = new SelectList(db.OPERACAO.OrderBy(p => p.descricao), "id_operacao", "descricao");
                        }
                    }
                    else
                    {
                        ViewBag.id_operacao = new SelectList(db.OPERACAO.OrderBy(p => p.descricao), "id_operacao", "descricao");

                    }


                    saldoAtual = movimentacoes.Where(p => p.dataPgto != null); ;


                    ViewBag.pgtoAberto = pgtoAberto;
                    if (pgtoAberto != null)
                    {
                        if (pgtoAberto.Equals("on"))
                        {

                            ViewBag.pgtoAberto = "checked";
                            movimentacoes = movimentacoes.Where(p => p.dataPgto == null);
                            order = "vencimento";
                        }

                    }

                    ViewBag.pgtoEfetuado = pgtoEfetuado;
                    if (pgtoEfetuado != null)
                    {
                        if (pgtoEfetuado.Equals("on"))
                        {

                            ViewBag.pgtoEfetuado = "checked";
                            movimentacoes = movimentacoes.Where(p => p.dataPgto != null);
                            order = "vencimento";
                        }

                    }

                    if (dataInicio != "" && dataFim != "")
                    {
                        if (dataInicio.Trim().Length > 1 && dataFim.Trim().Length > 1)
                        {
                            DateTime dtInicio = Convert.ToDateTime(dataInicio).Date;
                            DateTime dtFim = Convert.ToDateTime(dataFim).Date;
                            if (pesquisaData == null)
                                pesquisaData = "vencimento";
                            if (pesquisaData == "pagamento")
                                movimentacoes = movimentacoes.Where(p => EntityFunctions.TruncateTime(p.dataPgto) >= dtInicio.Date && EntityFunctions.TruncateTime(p.dataPgto) <= dtFim.Date);
                            else
                            {
                                if (pesquisaData == "vencimento")
                                    movimentacoes = movimentacoes.Where(p => EntityFunctions.TruncateTime(p.dataVenc) >= dtInicio.Date && EntityFunctions.TruncateTime(p.dataVenc) <= dtFim.Date);

                            }
                        }

                    }
                    else
                    {
                        if ((dataInicio != "" && dataFim == "") || (dataInicio == "" && dataFim != ""))
                            ViewBag.Msg = "Por favor preencha a data de início e data final";
                    }


                    if (order != null)
                    {
                        if (order.Trim().Equals("vencimento"))
                            movimentacoes = movimentacoes.OrderBy(p => p.dataVenc).ThenBy(x => x.valor);
                        if (order.Trim().Equals("operacao"))
                            movimentacoes = movimentacoes.OrderBy(p => p.id_operacao);
                        if (order.Trim().Equals("valor"))
                            movimentacoes = movimentacoes.OrderBy(p => p.valor);
                        if (order.Trim().Equals("pagamento"))
                            movimentacoes = movimentacoes.OrderBy(p => p.dataPgto);
                        if (order.Trim().Equals("banco"))
                            movimentacoes = movimentacoes.OrderBy(p => p.BANCO.nome);
                        if (order.Trim().Equals("destino"))
                            movimentacoes = movimentacoes.OrderBy(p => p.DESTINO.descricao);

                    }
                    else
                    {
                        movimentacoes = movimentacoes.OrderBy(p => p.dataVenc).ThenBy(x => x.dataPgto).ThenBy(x => x.valor);
                    }

                    #endregion


                    var totalCredito = saldoAtual.Where(p => p.OPERACAO.tipo == "C").Sum(p => p.valor);
                    var totalDebito = saldoAtual.Where(p => p.OPERACAO.tipo == "D").Sum(p => p.valor);

                    ViewBag.totalCredito = totalCredito;
                    ViewBag.totalDebito = totalDebito;



                    #region Variaveis de pesquisa - ViewBags
                    ViewBag.SaldoAtual = saldoAtual;
                    ViewBag.idoso = idoso;
                    ViewBag.dataInicio = dataInicio;
                    ViewBag.dataFim = dataFim;
                    ViewBag.TipoOperacao = TipoOperacao;
                    ViewBag.order = order;
                    ViewBag.pesquisaData = pesquisaData;
                    ViewBag.OperacaoID = id_operacao;
                    ViewBag.BancoID = id_banco;
                    ViewBag.pgtoAberto = pgtoAberto;
                    ViewBag.pgtoEfetuado = pgtoEfetuado;
                    ViewBag.DestinoID = idDestino;
                    ViewBag.tipoGrafico = grafico;
                    ViewBag.legenda = legenda;
                    #endregion

                    ViewBag.id_banco = new SelectList(from p in db.BANCO
                                                      select new
                                                      {
                                                          id_banco = p.id_banco,
                                                          nome = (p.nome + "-" + p.conta).Trim()
                                                      }, "id_banco", "nome").OrderBy(p => p.Text);

                    ViewBag.idDestino = new SelectList(db.DESTINO.OrderBy(p => p.descricao).OrderBy(p => p.descricao), "idDestino", "descricao");

                    #region REL SINTETICO
                    ///
                    DateTime dataHoje = DateTime.Now.Date;
                    var dadosSinteticos = movimentacoes.GroupBy(p => new { p.id_banco, p.id_operacao })
                        .Select(g => new MovimentoRelSimples
                        {
                            operacao = g.FirstOrDefault().OPERACAO.descricao,
                            bancoNome = g.FirstOrDefault().BANCO.nome + "-" + g.FirstOrDefault().BANCO.conta,
                            Debito = g.Where(p => p.OPERACAO.tipo == "D").Sum(p => p.valor),
                            Credito = g.Where(p => p.OPERACAO.tipo == "C").Sum(p => p.valor)
                        }).OrderBy(p => p.operacao).ToList();

                    ViewBag.dadosSinteticos = dadosSinteticos;
                    #endregion

                    #region REL Detalhado
                    ///

                    var dadosDetalhados = movimentacoes.GroupBy(p => new { p.id_banco, p.id_operacao, p.idDestino })
                        .Select(g => new MovimentoRelDetalhado
                        {
                            operacao = g.FirstOrDefault().OPERACAO.descricao,
                            destino = g.FirstOrDefault().DESTINO.descricao,
                            bancoNome = g.FirstOrDefault().BANCO.nome + "-" + g.FirstOrDefault().BANCO.conta,
                            Debito = g.Where(p => p.OPERACAO.tipo == "D").Sum(p => p.valor),
                            Credito = g.Where(p => p.OPERACAO.tipo == "C").Sum(p => p.valor)
                        }).OrderBy(p => p.operacao).ToList();

                    ViewBag.dadosDetalhados = dadosDetalhados;
                    #endregion



                    ///GRAFICOS
                    ///
                    //List<Highcharts> graficos = new List<Highcharts>();

                    //graficos.Add(GraficoCredito(movimentoes));
                    //graficos.Add(GraficoDebito(movimentoes));

                    ViewBag.Graficos = Graficos(movimentacoes, grafico,legenda);
                    ViewData["Movimentos"] = movimentacoes;
                    ViewBag.Tela = "Index";

                    //////////////////////////////////////////////////

                    if (Request.Form["Pesquisar"] != null)
                        return View(new MOVIMENTO() { listaMovimentos = movimentacoes.ToPagedList(numeroPagina, tamanhoPagina) });
                    else
                    {
                        if (Request.Form["ExcelAnalitico"] != null)
                            return Export(movimentacoes,saldoAtual);
                        else if (Request.Form["ExcelSintetico"] != null)
                            return ExportRelSintetico(dadosSinteticos);
                        else if (Request.Form["ExcelDetalhado"] != null)
                            return ExportRelDetalhado(dadosDetalhados);
                        else
                        {
                            return View(new MOVIMENTO() { listaMovimentos = movimentacoes.ToPagedList(numeroPagina, tamanhoPagina) });
                        }

                    }
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a consulta de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }

            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }

        }




        public ActionResult RelSintetico(IQueryable<MOVIMENTO> movimentacoes)
        {
            try
            {
                if (VerificaLoginConsMovimento())
                {

                    DateTime dataHoje = DateTime.Now.Date;
                    var movimentoes = movimentacoes.GroupBy(p => new { p.id_banco, p.id_operacao })
                        .Select(g => new MovimentoRelSimples
                        {
                            operacao = g.FirstOrDefault().OPERACAO.descricao,
                            bancoNome = g.FirstOrDefault().BANCO.nome + "-" + g.FirstOrDefault().BANCO.conta,
                            Debito = g.Where(p => p.OPERACAO.tipo == "D").Sum(p => p.valor),
                            Credito = g.Where(p => p.OPERACAO.tipo == "C").Sum(p => p.valor)
                        }).ToList();


                    if (Request.Form["Pesquisar"] != null)
                        return View(movimentoes);
                    else
                    {

                        return View(movimentoes);


                    }
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a consulta de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }

            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View();

            }

        }

        // Display decimal.Round parameters and the result.
        public decimal ShowDecimalRound(decimal Argument, int Digits)
        {
            decimal rounded = decimal.Round(Argument, Digits);

            //Console.WriteLine(dataFmt, Argument, Digits, rounded);
            return rounded;
        }


        //public dynamic GraficoDebito(IQueryable<MOVIMENTO> dadosGrafico1)
        //{


        //    decimal? valoresTotais = dadosGrafico1.Sum(p => p.valor);
        //    var movimentosDebito = dadosGrafico1.Where(p => p.OPERACAO.tipo == "D").GroupBy(p => new { p.id_operacao })
        //        .Select(g => new MovimentoGrafico1
        //        {
        //            operacao = g.FirstOrDefault().OPERACAO.descricao,
        //            porcentagem = g.Sum(p => p.valor)
        //        }).ToList();



        //    #region Débitos

        //    object[] listGrafico = new object[movimentosDebito.Count];
        //    int i = 0;
        //    foreach (var lista in movimentosDebito)
        //    {
        //        lista.porcentagem = (lista.porcentagem * 100) / valoresTotais;
        //        decimal valor = ShowDecimalRound((decimal)lista.porcentagem, 2);
        //        object obj = new object[] { lista.operacao, Convert.ToDouble(valor) };
        //        listGrafico[i] = obj;
        //        i++;
        //    }


        //    Series series = new Series();
        //    series.Type = ChartTypes.Pie;
        //    series.Name = "Operações Débito";
        //    Data data = new Data(listGrafico);



        //    Highcharts chart = new Highcharts("chart")
        //.InitChart(new Chart { PlotShadow = false })
        //.SetTitle(new Title { Text = "Débitos" })
        //.SetTooltip(new Tooltip { Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %'; }" })
        //.SetPlotOptions(new PlotOptions
        //{
        //    Pie = new PlotOptionsPie
        //    {
        //        AllowPointSelect = true,
        //        Cursor = Cursors.Pointer,
        //        DataLabels = new PlotOptionsPieDataLabels
        //        {
        //            Color = ColorTranslator.FromHtml("#000000"),
        //            ConnectorColor = ColorTranslator.FromHtml("#000000"),
        //            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %'; }"
        //        }
        //    }
        //})
        //.SetSeries(new Series
        //{
        //    Type = ChartTypes.Pie,
        //    Name = "Débitos",
        //    Data = data
        //});


        //    #endregion



        //    return chart;


        //}

        public dynamic Graficos(IQueryable<MOVIMENTO> dadosGrafico, string grafico,string legenda)
        {

            var tipografico = new ChartTypes();
            bool legend = false;

            if (legenda == "sim")
                legend = true;

            int i = 0;
            var chart = new Highcharts("chart");
            decimal? valoresTotais = dadosGrafico.Sum(p => p.valor);
            switch (grafico)
            {
                case "coluna":
                    tipografico = ChartTypes.Column;
                    var movimentosColuna = dadosGrafico.GroupBy(p => new { p.id_operacao, p.dataVenc.Value.Year, p.dataVenc.Value.Month })
                         .Select(g => new MovimentoGraficoColuna
                         {
                             mes = g.FirstOrDefault().dataVenc.Value.Month,
                             operacao = g.FirstOrDefault().OPERACAO.descricao,
                             porcentagem = g.Sum(p => p.valor)
                         }).ToList();
                    #region Coluna

                    i = 0;
                    string[] categorias = new string[movimentosColuna.GroupBy(p => p.mes).Select(g => new { mes = g.FirstOrDefault().mes }).ToList().Count()];
                    foreach (var lista in movimentosColuna.GroupBy(p => p.mes).Select(g => new { mes = g.FirstOrDefault().mes }).ToList())
                    {
                        categorias[i] = lista.mes.ToString();
                        i++;
                    }


                    i = 0;
                    Series[] series = new Series[movimentosColuna.GroupBy(p => p.operacao).Select(g => new { operacao = g.FirstOrDefault().operacao }).ToList().Count];
                    foreach (var lista in movimentosColuna.GroupBy(p => p.operacao).Select(g => new { operacao = g.FirstOrDefault().operacao }).OrderBy(p => p.operacao).ToList())
                    {
                        Series serie = new Series();


                        serie.Name = lista.operacao;
                        object[] obj = new object[categorias.Length];
                        for (int u = 0; u < categorias.Length; u++)
                        {
                            decimal? valoresTotaisCol = movimentosColuna.Where(p => p.mes.ToString() == categorias[u]).Sum(p => p.porcentagem);
                            foreach (var valores in movimentosColuna)
                            {
                                if (valores.mes.ToString() == categorias[u] && valores.operacao == lista.operacao)
                                {
                                    decimal? valor = (valores.porcentagem * 100) / valoresTotaisCol;
                                    obj[u] = valor;
                                }
                            }

                        }
                        serie.Data = new Data(obj);

                        series[i] = serie;

                        i++;
                    }

                    int j = 0;
                    foreach (var lista in categorias)
                    {
                        categorias[j] = categorias[j].Replace("1", "Jan").
                            Replace("2", "Fev").
                            Replace("3", "Mar").
                            Replace("4", "Abr").
                            Replace("5", "Mai").
                            Replace("6", "Jun").
                            Replace("7", "Jul").
                            Replace("8", "Ago").
                            Replace("9", "Set").
                            Replace("10", "Out").
                            Replace("11", "Nov").
                            Replace("12", "Dez");
                        j++;
                    }


                    chart = new Highcharts("chart")
                        .InitChart(new Chart { Type = ChartTypes.Column })
                        .SetTitle(new Title { Text = "Gráfico Comparativo Mensal" })
                        .SetSubtitle(new Subtitle { Text = "Operações" })
                        .SetXAxis(new XAxis { Categories = categorias, Title = new XAxisTitle { Text = "Meses" } })
                        .SetYAxis(new YAxis
                        {
                            Min = 0,
                            Title = new YAxisTitle { Text = "Operações (%)" }
                        })
                        //.SetLegend(new Legend
                        //{
                        //    Layout = Layouts.Horizontal,
                        //    Align = HorizontalAligns.Center,
                        //    VerticalAlign = VerticalAligns.Bottom,
                        //    X = 100,
                        //    Y = 70,
                        //    Floating = true,
                        //    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                        //    Shadow = true,
                        //})
                        .SetTooltip(new Tooltip
                        {
                            //Formatter = @"function() { return ''+ this.x +': '+ this.y +' mm'; }",
                            HeaderFormat = "<span style=\"font-size:10px\">Mês: {point.key}</span><table>",
                            PointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>" +
                                "<td style=\"padding:0\"><b>{point.y:.1f} %</b></td></tr>",
                            FooterFormat = "</table>",
                            Shared = true,
                            Crosshairs = new Crosshairs(true),
                            UseHTML = true


                        }
                            )
                        .SetPlotOptions(new PlotOptions
                        {
                            Column = new PlotOptionsColumn
                            {
                                BorderWidth = 0,
                                DataLabels = new PlotOptionsColumnDataLabels
                                {
                                    Enabled = true,
                                    Format = "{point.y:.1f}%",
                                },
                                ShowInLegend = legend
                            }
                            

                        })
                        .SetSeries(series);

                    #endregion
                    break;


                case "pizza":
                    tipografico = ChartTypes.Pie;
                    valoresTotais = dadosGrafico.Sum(p => p.valor);

                    var movimentosPizza = dadosGrafico.GroupBy(p => new { p.id_operacao })
                        .Select(g => new MovimentoGraficoPizza
                        {
                            operacao = g.FirstOrDefault().OPERACAO.descricao,
                            porcentagem = g.Sum(p => p.valor)
                        }).ToList();

                    #region Pizza

                    object[] listGraficoPizza = new object[movimentosPizza.Count];
                    i = 0;
                    foreach (var lista in movimentosPizza)
                    {
                        decimal? valor = (lista.porcentagem * 100) / valoresTotais;
                        object obj = new object[] { lista.operacao, valor };
                        listGraficoPizza[i] = obj;
                        i++;
                    }


                    Series seriesPizza = new Series();
                    seriesPizza.Type = ChartTypes.Pie;
                    seriesPizza.Name = "Operações";
                    Data data = new Data(listGraficoPizza);

                    chart = new Highcharts("chart")
                   .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false, Type = ChartTypes.Pie })
                   .SetTitle(new Title { Text = "Operações" })
                   .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.percentage:.1f}%</b>" })
                   .SetPlotOptions(new PlotOptions
                   {
                       Pie = new PlotOptionsPie
                       {
                           AllowPointSelect = true,
                           Cursor = Cursors.Pointer,
                           DataLabels = new PlotOptionsPieDataLabels
                           {
                               Enabled = true,
                               Format = "<b>{point.name}</b>: {point.percentage:.1f} %",
                               Style = "color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                               ConnectorColor = Color.Silver
                           },
                           ShowInLegend = legend
                       }
                   })
                   .SetSeries(new Series
                    {
                        Type = tipografico,
                        Name = "Operações",
                        Data = data
                    });




                    #endregion

                    break;

                default:
                    movimentosPizza = dadosGrafico.GroupBy(p => new { p.id_operacao })
                    .Select(g => new MovimentoGraficoPizza
                    {
                        operacao = g.FirstOrDefault().OPERACAO.descricao,
                        porcentagem = g.Sum(p => p.valor)
                    }).ToList();
                    tipografico = ChartTypes.Pie;

                    #region Pizza

                    listGraficoPizza = new object[movimentosPizza.Count];
                    i = 0;
                    foreach (var lista in movimentosPizza)
                    {
                        decimal? valor = (lista.porcentagem * 100) / valoresTotais;
                        object obj = new object[] { lista.operacao, valor };
                        listGraficoPizza[i] = obj;
                        i++;
                    }

                    seriesPizza = new Series();
                    seriesPizza.Type = ChartTypes.Pie;
                    seriesPizza.Name = "Operações";
                    data = new Data(listGraficoPizza);


                    chart = new Highcharts("chart")
                    .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false, Type = ChartTypes.Pie })
                    .SetTitle(new Title { Text = "Operações" })
                    .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.percentage:.1f}%</b>" })
                    .SetPlotOptions(new PlotOptions
                    {
                        Pie = new PlotOptionsPie
                        {
                            AllowPointSelect = true,
                            Cursor = Cursors.Pointer,
                            DataLabels = new PlotOptionsPieDataLabels
                            {
                                Enabled = true,
                                Format = "<b>{point.name}</b>: {point.percentage:.1f} %",
                                Style = "color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                                ConnectorColor = Color.Silver
                            },
                            ShowInLegend = legend
                        }
                    })
                    .SetSeries(new Series
                    {
                        Type = tipografico,
                        Name = "Operações",
                        Data = data
                    });


                    #endregion
                    break;


            }




            return chart;


        }



    


        private ActionResult Export(IQueryable<MOVIMENTO> movimentoes,IQueryable<MOVIMENTO> ListasaldoAtual)
        {
            try
            {
                if (VerificaLoginConsMovimento())
                {
                    var saldoAtualList = db.MOVIMENTO.Include(m => m.IDOSO).Include(m => m.OPERACAO);
                    Nullable<decimal> saldoAtual = 0;
                    foreach (var line in saldoAtualList)
                    {
                        if (line.OPERACAO.tipo == "D")
                            saldoAtual -= line.valor;
                        else
                        {
                            saldoAtual += line.valor;
                        }
                    }

                    List<MovimentoExport> listExport = new List<MovimentoExport>();
                    Nullable<decimal> saldo = 0;


                    foreach (var line in movimentoes)
                    {
                        MovimentoExport obj = new MovimentoExport();


                        

                        obj.operacao = line.OPERACAO.descricao;//operacao

                        obj.obs = line.obs;

                        if (line.DESTINO != null) // destino
                            obj.destino = line.DESTINO.descricao;
                        else
                        {
                            obj.destino = "";
                        }

                        string nomeIdoso = "";

                        if (line.IDOSO != null)
                        {
                            nomeIdoso = line.IDOSO.nome;
                        }

                        obj.idoso = nomeIdoso;//nome do idoso

                        obj.valor = String.Format("{0:C}", Convert.ToDouble(line.valor.ToString()));

                        if (line.OPERACAO.tipo == "C")
                            obj.tipoOperacao = "+";
                        else obj.tipoOperacao = "-"; 

                        if (line.dataVenc != null)
                            obj.dataVenc = line.dataVenc.Value.ToShortDateString();
                        else
                        {
                            obj.dataVenc = "";
                        }

                        if (line.dataPgto != null)
                            obj.dataPgto = line.dataPgto.Value.ToShortDateString();
                        else
                        {
                            obj.dataPgto = "";
                        }

                        obj.banco = line.BANCO.nome + line.BANCO.conta;
                        
                        
                        listExport.Add(obj);
                    }

                   
                    MovimentoExport objSaldoAtual = new MovimentoExport();

                    if (ListasaldoAtual != null)
                    {
                        objSaldoAtual.idoso = "Saldo Período";
                        objSaldoAtual.valor = (movimentoes.Where(p => p.OPERACAO.tipo == "C").Sum(p => p.valor) - movimentoes.Where(p => p.OPERACAO.tipo == "D").Sum(p => p.valor)).ToString();

                        objSaldoAtual.dataVenc = "Saldo Geral";
                        objSaldoAtual.dataPgto = (ListasaldoAtual.Where(p => p.OPERACAO.tipo == "C").Sum(p => p.valor) - ListasaldoAtual.Where(p => p.OPERACAO.tipo == "D").Sum(p => p.valor)).ToString();
                        
                    }else
                    {
                        objSaldoAtual.idoso = "Total";
                        objSaldoAtual.valor = (movimentoes.Where(p => p.OPERACAO.tipo == "C").Sum(p => p.valor) - movimentoes.Where(p => p.OPERACAO.tipo == "D").Sum(p => p.valor)).ToString();
                    }
                    listExport.Add(objSaldoAtual);


                    Response.ClearContent();
                    Response.AddHeader("content-disposition",
                        "attachment;filename=Movimentacao" +
                        DateTime.Now.ToShortDateString().Replace("/", "").Replace("-", "") + ".xls");
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    WriteTsv(listExport, Response.Output);
                    Response.End();

                    return RedirectToAction("Index");
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a consulta de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(movimentoes);

            }


        }

        //EXCEL RELATÓRIO SINTÉTICO
        private ActionResult ExportRelSintetico(IEnumerable<ASVP.Controllers.MovimentoController.MovimentoRelSimples> movimentoes)
        {
            try
            {
                if (VerificaLoginConsMovimento())
                {
                    List<MovimentoRelSimples> listExport = new List<MovimentoRelSimples>();
                    foreach (var line in movimentoes)
                    {
                        MovimentoRelSimples obj = new MovimentoRelSimples();


                        if (line.Credito == null)
                        {
                            line.Credito = 0;
                        }

                        if (line.Debito == null)
                        {
                            line.Debito = 0;
                        }

                        obj.operacao = line.operacao;
                        obj.bancoNome = line.bancoNome;
                        obj.Credito = line.Credito;
                        obj.Debito = line.Debito;
                        obj.saldo = line.Credito - line.Debito;
                        listExport.Add(obj);
                    }

                    MovimentoRelSimples objTotal = new MovimentoRelSimples();
                    objTotal.operacao = "TOTAL";

                    objTotal.Debito = movimentoes.Sum(p => p.Debito);
                    objTotal.Credito = movimentoes.Sum(p => p.Credito);
                    objTotal.saldo = movimentoes.Sum(p => p.Credito) - movimentoes.Sum(p => p.Debito);

                    listExport.Add(objTotal);




                    Response.ClearContent();
                    Response.AddHeader("content-disposition",
                        "attachment;filename=Movimentacao" +
                        DateTime.Now.ToShortDateString().Replace("/", "").Replace("-", "") + ".xls");
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    WriteTsv(listExport, Response.Output);
                    Response.End();

                    return RedirectToAction("Index");
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a consulta de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(movimentoes);

            }


        }

        //EXCEL RELATÓRIO DETALHADO
        private ActionResult ExportRelDetalhado(IEnumerable<ASVP.Controllers.MovimentoController.MovimentoRelDetalhado> movimentoes)
        {
            try
            {
                if (VerificaLoginConsMovimento())
                {
                    List<MovimentoRelDetalhado> listExport = new List<MovimentoRelDetalhado>();
                    foreach (var line in movimentoes)
                    {
                        MovimentoRelDetalhado obj = new MovimentoRelDetalhado();


                        if (line.Credito == null)
                        {
                            line.Credito = 0;
                        }

                        if (line.Debito == null)
                        {
                            line.Debito = 0;
                        }

                        obj.operacao = line.operacao;
                        obj.destino = line.destino;
                        obj.bancoNome = line.bancoNome;
                        obj.Credito = line.Credito;
                        obj.Debito = line.Debito;
                        obj.saldo = line.Credito - line.Debito;
                        listExport.Add(obj);
                    }

                    MovimentoRelDetalhado objTotal = new MovimentoRelDetalhado();
                    objTotal.operacao = "TOTAL";

                    objTotal.Debito = movimentoes.Sum(p => p.Debito);
                    objTotal.Credito = movimentoes.Sum(p => p.Credito);
                    objTotal.saldo = movimentoes.Sum(p => p.Credito) - movimentoes.Sum(p => p.Debito);

                    listExport.Add(objTotal);




                    Response.ClearContent();
                    Response.AddHeader("content-disposition",
                        "attachment;filename=Movimentacao" +
                        DateTime.Now.ToShortDateString().Replace("/", "").Replace("-", "") + ".xls");
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    WriteTsv(listExport, Response.Output);
                    Response.End();

                    return RedirectToAction("Index");
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a consulta de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return View(movimentoes);

            }


        }


        //LISTA IDOSOS AO DIGITAR NO CAMPO
        public ActionResult TagSearch(string term)
        {
            try
            {
                // Get Tags from database
                var idosos = from p in db.IDOSO
                             where p.nome.ToUpper().StartsWith(term.ToUpper())
                             select (SqlFunctions.StringConvert((double)p.id_idoso) + "-" + p.nome).Trim();

                return this.Json(idosos.ToList(),
                                JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;

                return null;
            }

        }


        //GERA PLANILHA EXCEL
        public void WriteTsv<T>(IEnumerable<T> data, TextWriter output)
        {
            try
            {
                PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
                foreach (PropertyDescriptor prop in props)
                {
                    output.Write(prop.DisplayName); // header
                    output.Write("\t");
                }
                output.WriteLine();
                foreach (T item in data)
                {
                    foreach (PropertyDescriptor prop in props)
                    {
                        output.Write(removeCE(prop.Converter.ConvertToString(prop.GetValue(item))));
                        output.Write("\t");
                    }
                    output.WriteLine();
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;


            }

        }


        private String removeCE(String str)
        {
            try
            {
                str = str.Replace("Á", "A").Replace("Â", "A").Replace("À", "A")
                        .Replace("Ã", "A").Replace("Ä", "A");
                str = str.Replace("á", "a").Replace("â", "a").Replace("à", "a")
                        .Replace("ã", "a").Replace("ä", "a");
                str = str.Replace("É", "E").Replace("Ê", "E").Replace("È", "E");
                str = str.Replace("é", "e").Replace("ê", "e").Replace("è", "e");
                str = str.Replace("Í", "I").Replace("Î", "I").Replace("Ì", "I")
                        .Replace("Ï", "I");
                str = str.Replace("í", "i").Replace("î", "i").Replace("ì", "i")
                        .Replace("ï", "i");
                str = str.Replace("Ó", "O").Replace("Ô", "O").Replace("Ò", "O")
                        .Replace("Õ", "O");
                str = str.Replace("ó", "o").Replace("ô", "o").Replace("ò", "o")
                        .Replace("õ", "o");
                str = str.Replace("Ú", "U").Replace("Ü", "U");
                str = str.Replace("ú", "u").Replace("ü", "u");
                str = str.Replace("Ç", "C").Replace("ç", "c");

                return str;
            }
            catch (Exception ex)
            {
                return "";
                // MessageBox.Show(")
            }
        }




        public class MovimentoExport
        {
            
            public string operacao { get; set; }
            public string obs { get; set; }
            public string destino { get; set; }
            public string idoso { get; set; }
            public string tipoOperacao { get; set; }
            public string valor { get; set; }
            public string dataVenc { get; set; }
            public string dataPgto { get; set; }
            public string banco { get; set; }
            
            
        }

        public class MovimentoRelSimples
        {
            [Display(Name = "Operação")]
            public string operacao { get; set; }
            [Display(Name = "Banco/Conta")]
            public string bancoNome { get; set; }
            [Display(Name = "Débito")]
            public decimal? Debito { get; set; }
            [Display(Name = "Crédito")]
            public decimal? Credito { get; set; }
            [Display(Name = "Saldo")]
            public decimal? saldo { get; set; }

        }

        public class MovimentoRelDetalhado
        {
            [Display(Name = "Operação")]
            public string operacao { get; set; }
            [Display(Name = "Destino")]
            public string destino { get; set; }
            [Display(Name = "Banco/Conta")]
            public string bancoNome { get; set; }
            [Display(Name = "Débito")]
            public decimal? Debito { get; set; }
            [Display(Name = "Crédito")]
            public decimal? Credito { get; set; }
            [Display(Name = "Saldo")]
            public decimal? saldo { get; set; }

        }






        //
        // GET: /Movimento/Details/5

        public ActionResult Details(int id)
        {
            try
            {
                if (VerificaLoginConsMovimento())
                {
                    MOVIMENTO movimento = db.MOVIMENTO.Find(id);
                    if (movimento == null)
                    {
                        return HttpNotFound();
                    }
                    return View(movimento);
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a consulta de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;

                return View();
            }

        }

        //
        // GET: /Movimento/Create

        public ActionResult Create(string dataInicio, string dataFim, string dataPagamento, string TipoOperacao, string id_operacao, string idoso, string order, int? pagina, string idDestino)
        {
            try
            {
                if (VerificaLoginCadMovimento())
                {
                    int tamanhoPagina = 40;
                    int numeroPagina = pagina ?? 1;




                    if (TipoOperacao != null)
                    {
                        if (TipoOperacao.Trim() != "tipo")
                            ViewBag.id_operacao = new SelectList(db.OPERACAO.Where(p => p.tipo == TipoOperacao).OrderBy(p => p.descricao),
                                "id_operacao", "descricao");
                        else
                        {
                            ViewBag.id_operacao = new SelectList(db.OPERACAO.OrderBy(p => p.descricao), "id_operacao", "descricao");
                        }

                    }
                    else
                    {
                        ViewBag.id_operacao = new SelectList(db.OPERACAO.OrderBy(p => p.descricao), "id_operacao", "descricao");
                    }


                    /*if(idDestino!= null)
                    {
                        if (!idDestino.Equals("Selecione um destino"))
                        {
                            int idDest = Convert.ToInt32(idDestino);
                            movimentos = movimentos.Where(p => p.idDestino == idDest).ToList();


                        }
                    }*/

                    ViewBag.id_banco = new SelectList(from p in db.BANCO
                                                      select new
                                                      {
                                                          id_banco = p.id_banco,
                                                          nome = (p.nome + "-" + p.conta).Trim()
                                                      }, "id_banco", "nome").OrderBy(p => p.Text);

                    ViewBag.idDestino = new SelectList(db.DESTINO.OrderBy(p => p.descricao).OrderBy(p => p.descricao), "idDestino", "descricao");



                    ViewBag.Tela = "Create";

                    ViewBag.dataInicio = dataInicio;
                    ViewBag.dataFim = dataFim;
                    ViewBag.dataPgto = dataPagamento;

                    return View(new MOVIMENTO() { listaMovimentos = BuscaMovimentosData(dataInicio,dataFim).ToPagedList(numeroPagina, tamanhoPagina) });
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de cadastro de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;

                return View();
            }


        }

        //
        // POST: /Movimento/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MOVIMENTO movimento, int? pagina, string dataInicio, string dataFim, string dataPagamento)
        {
            int tamanhoPagina = 40;
            int numeroPagina = pagina ?? 1;
            try
            {

                if (VerificaLoginCadMovimento())
                {

                    ViewBag.dataInicio = dataInicio;
                    ViewBag.dataFim = dataFim;
                    ViewBag.dataPgto = dataPagamento;
                    ViewBag.Tela = "Create";

                    if (Request.Form["Salvar"] != null)
                    {
                        #region Create
                        if (movimento.IDOSO != null)
                        {
                            if (movimento.IDOSO.nome != null)
                            {
                                movimento.id_idoso = Convert.ToInt32(movimento.IDOSO.nome.Split('-')[0]);

                                movimento.IDOSO = db.IDOSO.First(p => p.id_idoso == movimento.id_idoso);
                            }
                            else
                            {
                                movimento.IDOSO = null;
                            }
                        }
                        movimento.dataMov = DateTime.Now;
                        int idUser = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString()
                            .Replace("usuarioAsilo=", ""));
                        movimento.ADMINISTRADOR = db.ADMINISTRADOR.First(p => p.id_usuario == idUser);
                        if (movimento.id_operacao != null)
                        {
                            movimento.OPERACAO = db.OPERACAO.First(p => p.id_operacao == movimento.id_operacao);
                        }
                        if (movimento.obs != null)
                            movimento.obs = movimento.obs.ToUpper();


                        if (movimento.id_idoso != null)
                            ViewBag.id_idoso = new SelectList(db.IDOSO.OrderBy(p => p.nome), "id_idoso", "nome", movimento.id_idoso);
                        if (movimento.id_operacao != null)
                            ViewBag.id_operacao = new SelectList(db.OPERACAO.OrderBy(p => p.descricao), "id_operacao", "descricao", movimento.id_operacao);
                        ViewBag.id_banco = new SelectList(from p in db.BANCO
                                                          select new
                                                          {
                                                              id_banco = p.id_banco,
                                                              nome = (p.nome + "-" + p.conta).Trim()
                                                          }, "id_banco", "nome", movimento.id_banco).OrderBy(p => p.Text);

                        ViewBag.idDestino = new SelectList(db.DESTINO.OrderBy(p => p.descricao), "idDestino", "descricao");
                        if (movimento.OPERACAO != null)
                        {

                            if (movimento.OPERACAO.idoso == "S" && movimento.IDOSO == null)
                            {
                                ViewBag.Msg = "Este tipo de operação requer o nome de um idoso.";
                                return View(new MOVIMENTO() { listaMovimentos = BuscaMovimentosData(dataInicio, dataFim).ToPagedList(numeroPagina, tamanhoPagina) });
                            }
                        }
                        if (movimento.id_banco == null)
                        {
                            ViewBag.Msg = "Por favor, selecione o banco antes de cadastrar";
                            return View(new MOVIMENTO() { listaMovimentos = BuscaMovimentosData(dataInicio, dataFim).ToPagedList(numeroPagina, tamanhoPagina) });
                        }
                        ViewBag.idDestino = new SelectList(db.DESTINO.OrderBy(p => p.descricao), "idDestino", "descricao", movimento.idDestino);
                        
                        if (ModelState.IsValid)
                        {

                            db.MOVIMENTO.Add(movimento);
                            db.SaveChanges();

                            if (Request.Form["Salvar+"] != null)
                            {
                                movimento = new MOVIMENTO();
                                ViewBag.id_idoso = new SelectList(db.IDOSO.OrderBy(p => p.nome), "id_idoso", "nome", movimento.id_idoso);
                                ViewBag.id_operacao = new SelectList(db.OPERACAO.OrderBy(p => p.descricao), "id_operacao", "descricao",
                                    movimento.id_operacao);
                                ViewBag.MsgSucesso = "Movimentação cadastrada com sucesso.";
                                RedirectToAction("Create");

                            }
                            else
                            {
                                return RedirectToAction("Create");
                            }
                        }
                        #endregion
                    }

                    if(Request.Form["Atualizar"] != null)
                    {
                        if ((dataInicio != "" && dataInicio != null) && (dataFim != "" && dataFim != null))
                        {
                            if (dataPagamento != "" && dataPagamento != null)
                            {
                                List<MOVIMENTO> list = BuscaMovimentosData(dataInicio, dataFim);

                                foreach (var item in list)
                                {
                                    item.dataPgto = Convert.ToDateTime(dataPagamento);
                                }

                                db.SaveChanges();
                            }
                            else ViewBag.Msg = "Preencha a data de pagamento antes de atualizar os movimentos.";
                        }
                        else ViewBag.Msg = "Preencha a data de inicio e data final antes de atualizar os movimentos.";

                    }
                    

                    if (Request.Form["ExcelAnalitico"] != null)
                        return Export(BuscaMovimentosData(dataInicio,dataFim).AsQueryable(),null);
                    else
                        return View(new MOVIMENTO() { listaMovimentos = BuscaMovimentosData(dataInicio,dataFim).ToPagedList(numeroPagina, tamanhoPagina) });
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de cadastro de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;

                return View(new MOVIMENTO() { listaMovimentos = BuscaMovimentosData(dataInicio,dataFim).ToPagedList(numeroPagina, tamanhoPagina) });
            }

        }

        //
        // GET: /Movimento/Edit/5

        public ActionResult Edit(int id, string TipoOperacao, int pagina)
        {
            try
            {
                if (VerificaLoginCadMovimento())
                {
                    MOVIMENTO movimento = db.MOVIMENTO.Find(id);
                    if (TipoOperacao != null)
                    {
                        if (TipoOperacao.Trim() != "tipo")
                            ViewBag.id_operacao = new SelectList(db.OPERACAO.Where(p => p.tipo == TipoOperacao).OrderBy(p => p.descricao),
                                "id_operacao", "descricao");
                        else
                        {
                            ViewBag.id_operacao = new SelectList(db.OPERACAO.OrderBy(p => p.descricao), "id_operacao", "descricao");
                        }
                    }
                    else
                    {
                        ViewBag.id_operacao = new SelectList(db.OPERACAO.OrderBy(p => p.descricao), "id_operacao", "descricao");
                    }

                    if (movimento.IDOSO != null)
                        movimento.IDOSO.nome = movimento.id_idoso.ToString() + "-" + movimento.IDOSO.nome;

                    if (movimento == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.id_idoso = new SelectList(db.IDOSO.OrderBy(p => p.nome), "id_idoso", "nome", movimento.id_idoso);
                    ViewBag.id_banco = new SelectList(from p in db.BANCO
                                                      select new
                                                      {
                                                          id_banco = p.id_banco,
                                                          nome = (p.nome + "-" + p.conta).Trim()
                                                      }, "id_banco", "nome", movimento.id_banco).OrderBy(p => p.Text);

                    ViewBag.idDestino = new SelectList(db.DESTINO.OrderBy(p => p.descricao), "idDestino", "descricao", movimento.idDestino);
                    ViewBag.Pagina = pagina;
                    return View(movimento);
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de cadastro de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;

                return View();
            }

        }

        //
        // POST: /Movimento/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MOVIMENTO movimento, int page)
        {
            try
            {
                if (VerificaLoginCadMovimento())
                {
                    if (movimento.IDOSO.nome != null)
                    {
                        movimento.id_idoso = Convert.ToInt32(movimento.IDOSO.nome.Split('-')[0]);
                        movimento.IDOSO = db.IDOSO.First(p => p.id_idoso == movimento.id_idoso);
                    }
                    else
                    {
                        movimento.IDOSO = null;
                    }
                    movimento.id_operacao = movimento.OPERACAO.id_operacao;
                    movimento.id_banco = movimento.BANCO.id_banco;
                    movimento.BANCO = db.BANCO.First(p => p.id_banco == movimento.id_banco);
                    movimento.OPERACAO = db.OPERACAO.First(p => p.id_operacao == movimento.id_operacao);
                    if (movimento.DESTINO.idDestino != null)
                    {
                        movimento.idDestino = movimento.DESTINO.idDestino;
                        movimento.DESTINO = null;
                    }
                    else
                    {
                        movimento.DESTINO = null;

                    }

                    if (movimento.obs != null)
                        movimento.obs = movimento.obs.ToUpper();



                    int idUser = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString()
                        .Replace("usuarioAsilo=", ""));
                    movimento.ADMINISTRADOR = db.ADMINISTRADOR.First(p => p.id_usuario == idUser);
                    movimento.id_usuario = db.ADMINISTRADOR.First(p => p.id_usuario == idUser).id_usuario;
                    ViewBag.id_operacao = new SelectList(db.OPERACAO.OrderBy(p => p.descricao), "id_operacao", "descricao", movimento.id_operacao);
                    ViewBag.id_banco = new SelectList(from p in db.BANCO
                                                      select new
                                                      {
                                                          id_banco = p.id_banco,
                                                          nome = (p.nome + "-" + p.conta).Trim()
                                                      }, "id_banco", "nome", movimento.id_banco).OrderBy(p => p.Text);

                    ViewBag.idDestino = new SelectList(db.DESTINO.OrderBy(p => p.descricao), "idDestino", "descricao", movimento.idDestino);
                    if (movimento.OPERACAO.idoso == "S" && movimento.IDOSO == null)
                    {
                        ViewBag.Msg = "Este tipo de operação requer o nome de um idoso.";
                        return View(movimento);
                    }


                    if (ModelState.IsValid)
                    {
                        db.Entry(movimento).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Create", new { pagina = page });
                    }
                    //ViewBag.id_idoso = new SelectList(db.IDOSO, "id_idoso", "nome", movimento.id_idoso);

                    return View(movimento);
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de cadastro de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;

                return View();
            }

        }

        //
        // GET: /Movimento/Delete/5

        public ActionResult Delete(int id)
        {
            try
            {
                if (VerificaLoginCadMovimento())
                {
                    MOVIMENTO movimento = db.MOVIMENTO.Find(id);
                    if (movimento == null)
                    {
                        return HttpNotFound();
                    }
                    return View(movimento);
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de cadastro de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;

                return View();
            }

        }

        //
        // POST: /Movimento/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                if (VerificaLoginCadMovimento())
                {
                    MOVIMENTO movimento = db.MOVIMENTO.Find(id);
                    db.MOVIMENTO.Remove(movimento);
                    db.SaveChanges();
                    return RedirectToAction("Create");
                }
                else
                {

                    TempData["alertMessage"] = "Você precisa de permissão para acessar a tela de cadastro de movimentos.";
                    return RedirectToAction("Index", "Home", TempData["alertMessage"]);
                }

            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;

                return View();
            }

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }



        public bool VerificaLoginCadMovimento()
        {

            if (Request.Cookies["usuarioAsilo"] != null)
            {
                int id = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString().Replace("usuarioAsilo=", ""));
                if (db.ADMINISTRADOR.Find(id).LancMovimento == "S")
                    return true;
                else return false;
            }
            else
            {
                return false;

            }
        }

        public bool VerificaLoginConsMovimento()
        {

            if (Request.Cookies["usuarioAsilo"] != null)
            {
                int id = Convert.ToInt32(Request.Cookies["usuarioAsilo"].Value.ToString().Replace("usuarioAsilo=", ""));
                if (db.ADMINISTRADOR.Find(id).consMovimento == "S")
                    return true;
                else return false;
            }
            else
            {
                return false;

            }
        }



        public List<MOVIMENTO> BuscaMovimentosMes()
        {
            try
            {
                var movimentos = db.MOVIMENTO.Where(p => p.dataMov.Value.Month == DateTime.Now.Month -1 &&
            p.dataMov.Value.Year == DateTime.Now.Year).OrderBy(p => p.dataVenc).ThenBy(x => x.dataPgto).ThenBy(x => x.valor).ToList();
                return movimentos;
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return null;
            }



        }
        public List<MOVIMENTO> BuscaMovimentosData(string dataInicio,string dataFim)
        {
            try
            {
                var movimentos = db.MOVIMENTO.Where(p => p.dataVenc.Value.Year == DateTime.Now.Year).ToList();
                if (dataInicio != null && dataFim != null)
                {
                    if (dataInicio != "" && dataFim != "")
                    {
                        if (dataInicio.Trim().Length > 1 && dataFim.Trim().Length > 1)
                        {

                            DateTime dtInicio = Convert.ToDateTime(dataInicio).Date;
                            DateTime dtFim = Convert.ToDateTime(dataFim).Date;
                            movimentos = db.MOVIMENTO.Where(p => EntityFunctions.TruncateTime(p.dataVenc) >= dtInicio.Date && EntityFunctions.TruncateTime(p.dataVenc) <= dtFim.Date).OrderBy(p => p.dataVenc).ThenBy(x => x.dataPgto).ThenBy(x => x.valor).ToList();

                        }

                    }
                }
                else
                {
                    movimentos = db.MOVIMENTO.Where(p => p.dataVenc.Value.Year == DateTime.Now.Year).OrderByDescending(p => p.dataMov).ThenBy(x => x.dataVenc).ThenBy(x => x.dataPgto).ThenBy(x => x.valor).ToList();
                }
                return movimentos;
                
            }
            catch (Exception e)
            {
                ViewBag.Msg = e.Message;
                return null;
            }



        }

        public ActionResult RelCaixa(string dataInicio, string dataFim, string pgtoAberto, string pgtoEfetuado, int? pagina, string TipoOperacao)
        {

            int tamanhoPagina = 40;
            int numeroPagina = pagina ?? 1;

            var movimentos = db.MOVIMENTO.Where(p => p.id_banco == 10).Include(m => m.IDOSO).Include(m => m.OPERACAO).Include(m => m.BANCO).Include(m => m.DESTINO);

            ViewBag.dataInicio = dataInicio;
            ViewBag.dataFim = dataFim;


            ViewBag.pgtoAberto = pgtoAberto;
            if (pgtoAberto != null)
            {
                if (pgtoAberto.Equals("on"))
                {

                    ViewBag.pgtoAberto = "checked";
                    movimentos = movimentos.Where(p => p.dataPgto == null);
                }

            }

            if (TipoOperacao != null)
            {
                if (TipoOperacao.Trim() != "tipo")
                {

                    movimentos = movimentos.Where(p => p.OPERACAO.tipo == TipoOperacao);
                }
            }
            ViewBag.TipoOperacao = TipoOperacao;

            ViewBag.pgtoEfetuado = pgtoEfetuado;
            if (pgtoEfetuado != null)
            {
                if (pgtoEfetuado.Equals("on"))
                {

                    ViewBag.pgtoEfetuado = "checked";
                    movimentos = movimentos.Where(p => p.dataPgto != null);
                }

            }

            int dias = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            if (dataInicio == null && dataFim == null)
            {
                dataInicio = "01" + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" +
                             DateTime.Now.Year.ToString();
                dataFim = dias.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') +
                          "/" + DateTime.Now.Year.ToString();
            }

            if (dataInicio != "" && dataFim != "")
            {
                if (dataInicio.Trim().Length > 1 && dataFim.Trim().Length > 1)
                {
                    DateTime dtInicio = Convert.ToDateTime(dataInicio).Date;
                    DateTime dtFim = Convert.ToDateTime(dataFim).Date;
                    movimentos = movimentos.Where(p => EntityFunctions.TruncateTime(p.dataPgto) >= dtInicio.Date && EntityFunctions.TruncateTime(p.dataPgto) <= dtFim.Date);

                }

            }
            else
            {
                if ((dataInicio != "" && dataFim == "") || (dataInicio == "" && dataFim != ""))
                    ViewBag.Msg = "Por favor preencha a data de início e data final";
            }


            movimentos = movimentos.OrderBy(p => p.dataVenc).ThenBy(x => x.valor);

            if (Request.Form["Rel"] != null)
            {
                FileResult relatorio = RelCaixaAsilo(movimentos,dataInicio,dataFim);
                return relatorio;

            }
            ViewData["Movimentos"] = movimentos;
            return View(movimentos.ToPagedList(numeroPagina, tamanhoPagina));
        }

        public FileResult RelCaixaAsilo(IQueryable<MOVIMENTO> movimentos,string dataInicial,string dataFim)
        {

            //IDOSO idoso = db.IDOSO.Find(id);

            Document documento = new Document();
            string pCaminhoArquivoPDF = Server.MapPath("~/Relatorio/RelCaixaAsilo.pdf");

            if (System.IO.File.Exists(pCaminhoArquivoPDF))
            {
                System.IO.File.Delete(pCaminhoArquivoPDF);
            }
            PdfWriter writer = PdfWriter.GetInstance(documento, new FileStream(pCaminhoArquivoPDF, FileMode.Append));

            try
            {
                documento.Open();
                PdfContentByte cb = writer.DirectContent;

                cb.MoveTo(documento.PageSize.Width - 30, 30);
                // x = 297.5 - y = 210.5
                cb.LineTo(documento.PageSize.Width - 30, documento.PageSize.Height - 30);
                cb.Stroke();

                cb.MoveTo(documento.PageSize.Width - 30, documento.PageSize.Height - 30);
                // x = 297.5 - y = 210.5
                cb.LineTo(30, documento.PageSize.Height - 30);
                cb.Stroke();


                cb.MoveTo(30, 30); // x = 10 - y = 210.5
                cb.LineTo(documento.PageSize.Width - 30, 30);
                cb.Stroke();

                cb.MoveTo(30, 30); // x = 10 - y = 210.5
                cb.LineTo(30, documento.PageSize.Height - 30);
                cb.Stroke();

                System.Drawing.Image image =
                    System.Drawing.Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "/Images/asilo_logopdf.png");
                iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(image,
                    System.Drawing.Imaging.ImageFormat.Png);
                pdfImage.SetAbsolutePosition(40f, Convert.ToSingle(documento.PageSize.Height - (95)));
                documento.Add(pdfImage);

                ////TÍTULO FICHA CADASTRAL DE IDOSO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 18);
                cb.BeginText();
                cb.SetColorFill(BaseColor.BLACK);
                if(dataInicial != null && dataInicial != "")
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Capa do Caixa "+dataInicial +" - "+dataFim, 160f,
                    Convert.ToSingle(documento.PageSize.Height - (70)), 0f);
                else
                    cb.ShowTextAligned(Element.ALIGN_LEFT, "Capa do Caixa (Período completo)" , 160f,
                    Convert.ToSingle(documento.PageSize.Height - (70)), 0f);

                cb.EndText();
                cb.Stroke();

                ////////////////////////////////////////////////////////////////////////////


                cb.MoveTo(documento.PageSize.Width - 40, 40);
                // x = 297.5 - y = 210.5
                cb.LineTo(documento.PageSize.Width - 40, documento.PageSize.Height - 100);
                cb.Stroke();

                cb.MoveTo(40, 40); // x = 10 - y = 210.5
                cb.LineTo(documento.PageSize.Width - 40, 40);
                cb.Stroke();

                cb.MoveTo(40, 40); // x = 10 - y = 210.5
                cb.LineTo(40, documento.PageSize.Height - 100);
                cb.Stroke();

                int pos = 60;


                #region HEADER


                cb.SetColorStroke(BaseColor.DARK_GRAY);



                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40),
                    Convert.ToSingle(documento.PageSize.Height - (40 + pos)));
                cb.Stroke();

                pos += 50;


                //HEADER
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Operação", 45f,
                    Convert.ToSingle(documento.PageSize.Height - (pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Descrição", 185f,
                    Convert.ToSingle(documento.PageSize.Height - (pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Destino", 335f,
                    Convert.ToSingle(documento.PageSize.Height - (pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Valor", 410f,
                    Convert.ToSingle(documento.PageSize.Height - (pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Vencimento", 445f,
                    Convert.ToSingle(documento.PageSize.Height - (pos)), 0f);
                cb.EndText();
                cb.Stroke();

                cb.BeginText();
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Pagamento", 505f,
                    Convert.ToSingle(documento.PageSize.Height - (pos)), 0f);
                cb.EndText();
                cb.Stroke();

                pos += 7;
                cb.MoveTo(40, Convert.ToSingle(documento.PageSize.Height - (pos))); // _
                cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40),
                    Convert.ToSingle(documento.PageSize.Height - (pos)));
                cb.Stroke();



                #endregion


                pos -= 35;
                int[] posProx = new int[6];
                foreach (var item in movimentos)
                {

                    cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 6);


                    posProx[0] = TrataCampo(45f, Convert.ToSingle(documento.PageSize.Height), 30, cb, documento,
                        pos, item.OPERACAO.descricao, BaseColor.BLACK);

                    if (item.obs != null)
                        posProx[1] = TrataCampo(155f, Convert.ToSingle(documento.PageSize.Height), 50, cb, documento,
                            pos, item.obs, BaseColor.BLACK);
                    else posProx[1] = 0;

                    if (item.DESTINO != null)
                        posProx[2] = TrataCampo(335f, Convert.ToSingle(documento.PageSize.Height), 18, cb, documento,
                            pos, item.DESTINO.descricao, BaseColor.BLACK);
                    else posProx[2] = 0;

                    if (item.valor != null)
                    {
                        if (item.OPERACAO.tipo == "D")
                        {

                            posProx[3] = TrataCampo(415f, Convert.ToSingle(documento.PageSize.Height), 15, cb, documento,
                           pos, "(-) " + item.valor.ToString(), BaseColor.BLACK);
                        }
                        else
                        {

                            posProx[3] = TrataCampo(415f, Convert.ToSingle(documento.PageSize.Height), 15, cb, documento,
                           pos, "  " + item.valor.ToString(), BaseColor.BLACK);
                        }

                    }
                    else posProx[3] = 0;

                    if (item.dataVenc != null)
                        posProx[4] = TrataCampo(455f, Convert.ToSingle(documento.PageSize.Height), 15, cb, documento,
                            pos, item.dataVenc.Value.ToShortDateString(), BaseColor.BLACK);
                    else
                        posProx[4] = 0;

                    if (item.dataPgto != null)
                        posProx[5] = TrataCampo(515f, Convert.ToSingle(documento.PageSize.Height), 15, cb, documento,
                            pos, item.dataPgto.Value.ToShortDateString(), BaseColor.BLACK);
                    else
                        posProx[5] = 0;

                    pos = posProx.Max();



                    #region proximaPagina
                    if (pos > 720)
                    {
                        documento.NewPage();

                        cb.MoveTo(documento.PageSize.Width - 40, 40);
                        // x = 297.5 - y = 210.5
                        cb.LineTo(documento.PageSize.Width - 40, documento.PageSize.Height - 40);
                        cb.Stroke();

                        cb.MoveTo(documento.PageSize.Width - 40, documento.PageSize.Height - 40);
                        // x = 297.5 - y = 210.5
                        cb.LineTo(40, documento.PageSize.Height - 40);
                        cb.Stroke();


                        cb.MoveTo(40, 40); // x = 10 - y = 210.5
                        cb.LineTo(documento.PageSize.Width - 40, 40);
                        cb.Stroke();

                        cb.MoveTo(40, 40); // x = 10 - y = 210.5
                        cb.LineTo(40, documento.PageSize.Height - 40);
                        cb.Stroke();



                        pos = 5;


                        #region HEADER


                        cb.SetColorStroke(BaseColor.DARK_GRAY);




                        //HEADER
                        cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                        cb.BeginText();
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, "Operação", 45f,
                            Convert.ToSingle(documento.PageSize.Height - (50 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();

                        cb.BeginText();
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, "Descrição", 185f,
                            Convert.ToSingle(documento.PageSize.Height - (50 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();

                        cb.BeginText();
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, "Destino", 335f,
                            Convert.ToSingle(documento.PageSize.Height - (50 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();

                        cb.BeginText();
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, "Valor", 410f,
                            Convert.ToSingle(documento.PageSize.Height - (50 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();

                        cb.BeginText();
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, "Vencimento", 445f,
                            Convert.ToSingle(documento.PageSize.Height - (50 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();

                        cb.BeginText();
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.ShowTextAligned(Element.ALIGN_LEFT, "Pagamento", 505f,
                            Convert.ToSingle(documento.PageSize.Height - (50 + pos)), 0f);
                        cb.EndText();
                        cb.Stroke();


                        #endregion



                        pos += 15;


                    }
                    else
                    {

                        cb.MoveTo(40f, Convert.ToSingle(documento.PageSize.Height - (40 + pos + 5))); // _
                        cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40),
                            Convert.ToSingle(documento.PageSize.Height - (40 + pos + 5)));
                        cb.Stroke();

                        /*cb.MoveTo(40f, Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // |
                        cb.LineTo(Convert.ToSingle(40), Convert.ToSingle(documento.PageSize.Height - (40 + pos)));
                        cb.Stroke();

                        cb.MoveTo(Convert.ToSingle(documento.PageSize.Width - 40),
                            Convert.ToSingle(documento.PageSize.Height - (40 + pos))); // |
                        cb.LineTo(Convert.ToSingle(documento.PageSize.Width - 40),
                            Convert.ToSingle(documento.PageSize.Height - (40 + pos)));*/

                        pos += 5;



                    }
                    #endregion

                }

                //TOTAL

                decimal? debito = movimentos.Where(p => p.OPERACAO.tipo == "D").Sum(p => p.valor);
                decimal? credito = movimentos.Where(p => p.OPERACAO.tipo == "C").Sum(p => p.valor);
                if (debito == null)
                    debito = 0;
                if (credito == null)
                    credito = 0;
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                BaseColor cor = BaseColor.BLACK;
                if (credito- debito < 0)
                    cor = BaseColor.BLACK;

                TrataCampo(335f, Convert.ToSingle(documento.PageSize.Height), 30, cb, documento,
                        pos, "TOTAL", cor);

                if(Convert.ToDecimal(credito - debito) < 0)
                    TrataCampo(410f, Convert.ToSingle(documento.PageSize.Height), 18, cb, documento,
                            pos, "(-) " + string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", Convert.ToDecimal(credito - debito)), cor);
                else
                    TrataCampo(410f, Convert.ToSingle(documento.PageSize.Height), 18, cb, documento,
                            pos, string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", Convert.ToDecimal(credito - debito)), cor);


                documento.Close();

                writer.Close();






                return File(pCaminhoArquivoPDF, "application/pdf");

            }
            catch (Exception e)
            {
                //MessageBox.Show(e.StackTrace);
                if (documento.IsOpen())
                    documento.Close();
                return null;
            }
        }

        public int TrataCampo(float horizontal, float vertical, int tamanho, PdfContentByte cb, Document documento, int pos, string item, BaseColor color)
        {
            decimal qtdLinhas = Math.Ceiling(Convert.ToDecimal(item.Trim().Length) / Convert.ToDecimal(tamanho));
            int posicaoInicial = 0;
            int aux = 0;
            for (int i = 0; i < Convert.ToInt32(qtdLinhas); i++)
            {
                aux = posicaoInicial;
                if (item.Trim().Length - posicaoInicial <= tamanho)
                {
                    if (posicaoInicial > 0)
                    {
                        aux -= 1;
                        posicaoInicial -= 1;
                    }
                    else
                        aux = posicaoInicial;
                    cb.BeginText();
                    cb.SetColorFill(color);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, item.Trim().Substring(aux, item.Trim().Length - posicaoInicial).Trim(), horizontal,
                        vertical - (50 + pos), 0f);
                    cb.EndText();
                    cb.Stroke();

                }
                else
                {
                    cb.BeginText();
                    cb.SetColorFill(color);
                    cb.ShowTextAligned(Element.ALIGN_LEFT, item.Trim().Substring(posicaoInicial, tamanho).Trim(), horizontal,
                        vertical - (50 + pos), 0f);
                    cb.EndText();
                    cb.Stroke();
                    posicaoInicial += tamanho;

                }
                pos += 8;
            }

            return pos;

        }

      
    }
}