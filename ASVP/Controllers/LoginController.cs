﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASVP.Models;

namespace ASVP.Controllers
{
    public class LoginController : Controller
    {
        private AsiloEntities db = new AsiloEntities();

        public ActionResult Index(String login, String senha)
        {
            try
            {
                if (login != null)
                {
                    TempData["alertMessage"] = "";
                    List<ADMINISTRADOR> list = db.ADMINISTRADOR.Where(a => a.username == login).ToList();

                    if (list.Count > 0)
                    {
                        if (list[0].senha == senha)
                        {
                            Session["administrador"] = true;
                            Session["usuarioAsilo"] = true;

                            HttpCookie cookieUser = new HttpCookie("usuarioAsilo");
                            cookieUser["usuarioAsilo"] = list[0].id_usuario.ToString();
                            cookieUser.Expires = DateTime.Now.AddDays(1d);
                            Response.Cookies.Add(cookieUser);


                            return RedirectToAction("index", "Home");
                        }
                        else
                        {
                            senha = "";
                            if (Request.Cookies["usuarioAsilo"] != null)
                            {
                                Request.Cookies["usuarioAsilo"].Value = null;
                            }
                            TempData["alertMessage"] = "Senha Incorreta!";
                            @ViewBag.Login = login;
                            return View();
                        }
                    }
                    else
                    {
                        if (Request.Cookies["usuarioAsilo"]!= null)
                        {
                            Request.Cookies["usuarioAsilo"].Value = null;
                        }
                        TempData["alertMessage"] = "Usuário Incorreto!";
                        return View();
                    }
                    
                }
                else
                {
                    return View();
                }
            }
            catch (Exception exception)
            {
                return View();
            }
            
        }

        public ActionResult Logout() {

            if (Request.Cookies["usuarioAsilo"] != null)
            {
                HttpCookie myCookie = new HttpCookie("usuarioAsilo");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            return RedirectToAction("index", "Login");
        }

    }

}
       


