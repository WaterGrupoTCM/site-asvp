
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace ASVP.Models
{

using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
public partial class IDOSO
{

    public IDOSO()
    {

        this.BENEFICIO_IDOSO = new HashSet<BENEFICIO_IDOSO>();

        this.CONTATO_IDOSO = new HashSet<CONTATO_IDOSO>();

        this.MOVIMENTO = new HashSet<MOVIMENTO>();

    }

    public int id_idoso { get; set; }
    [Display(Name = "Idoso")]
    [MaxLength(40)]
    public string nome { get; set; }
    [Display(Name = "CPF")]
    public string cpf { get; set; }
    [Display(Name = "RG")]
    [MaxLength(12, ErrorMessage = "O tamanho n�o pode ser maior que 12")]
    public string rg { get; set; }
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    [Display(Name = "Data Emiss�o")]
    public Nullable<System.DateTime> dtEmissaoRG { get; set; }
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    [Display(Name = "Data Nasc.")]
    //[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    public Nullable<System.DateTime> dataNasc { get; set; }

    [Display(Name = "Aposentado")]
    public string aposentado { get; set; }



    [Display(Name = "Plano de Sa�de")]
    public string planosaude { get; set; }



    [Display(Name = "Data Entrada")]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    public Nullable<System.DateTime> dataEntrada { get; set; }


    [Display(Name = "Ativo")]
    public string ativo { get; set; }

    [Display(Name = "Vagas SUAS")]
    public string vagasuas { get; set; }

    [Display(Name = "Data �bito")]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    public Nullable<System.DateTime> dataObito { get; set; }

    [Display(Name = "T�tulo Eleitor")]
    public string tituloEleitor { get; set; }
    [Display(Name = "N�mero Certid�o Nasc.")]
    public string certNascN { get; set; }
    [Display(Name = "Livro Certid�o Nasc.")]
    public string certNascL { get; set; }
    [Display(Name = "Folha Certid�o Nasc.")]
    public string certNascF { get; set; }
    [Display(Name = "Cart�rio")]
    public string cartorio { get; set; }
    [Display(Name = "Posto de Sa�de")]
    public string postoSaude { get; set; }
    [Display(Name = "Cart�o SUS")]
    public string cartaoSus { get; set; }
    [Display(Name = "Nome do Pai")]
    [MaxLength(40)]
    public string pai { get; set; }
    [Display(Name = "Nome da M�e")]
    [MaxLength(40)]
    public string mae { get; set; }
    [Display(Name = "Naturalidade")]
    public string naturalidade { get; set; }
    [Display(Name = "UF")]
    public string uf { get; set; }
    [Display(Name = "Estado Civil")]
    public string estadoCivil { get; set; }
    [Display(Name = "Sexo")]
    public string sexo { get; set; }

    [MaxLength(40)]
    public string Procurador { get; set; }
    [Display(Name = "N�mero")]
    public string ProcuracaoN { get; set; }
    [Display(Name = "Livro")]
    public string ProcuracaoL { get; set; }
    [Display(Name = "Folha")]
    public string ProcuracaoF { get; set; }

    [Display(Name = "Data")]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    public Nullable<System.DateTime> dataProcuracao { get; set; }
    [Display(Name = "Curador")]
    [MaxLength(40)]
    public string curador { get; set; }
    [Display(Name = "Perfil")]
    public string perfil { get; set; }
    [Display(Name = "Dem�ncia")]
    public string demencia { get; set; }

    [Display(Name = "Grau Depend�ncia")]
    public Nullable<int> gDependencia { get; set; }

    [Display(Name = "Funer�ria")]
    public string funeraria { get; set; }

    [Display(Name = "Nome")]
    public string nomeFuneraria { get; set; }

    [Display(Name = "Titular")]
    [MaxLength(40)]
    public string titularFuneraria { get; set; }

    [Display(Name = "Leito")]
    public string leitoPs { get; set; }

    [Display(Name = "Tam. Cal�a")]
    public string tamanhoCalca { get; set; }

    [Display(Name = "Tam. Camisa")]
    public string tamanhoCamisa { get; set; }

    [Display(Name = "Tam. Cal�ado")]
    public string tamanhoCalcado { get; set; }

    [Display(Name = "Informa��es Complementares")]
    public string obs { get; set; }

    [Display(Name = "Tipo Sangue")]
    public string tpSangue { get; set; }
	


    public virtual ICollection<BENEFICIO_IDOSO> BENEFICIO_IDOSO { get; set; }

    public virtual ICollection<CONTATO_IDOSO> CONTATO_IDOSO { get; set; }

    public virtual ICollection<MOVIMENTO> MOVIMENTO { get; set; }

}

}
